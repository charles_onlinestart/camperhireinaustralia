<?php
/**
 * @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 * Joomla! is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jdoc:include type="head" /><?php setcookie("mobile","m", time()+3600, "/",".yourdomain.com"); ?>
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/system.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/system/css/general.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/styles/style.css" type="text/css" /><meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="main_wrapper">
  <div class="header_wrapper">
    <div class="logo"><a href="<?php echo $this->baseurl ?>" title=""><img src="<?php echo $this->baseurl ?>/templates/<?php echo $this->template ?>/images/logo.png" alt="<?php echo JText::_('Camper Hire in Australia'); ?>" /></a></div>
   <?php if($this->countModules('topMenu')) : ?>
    <div class="main_nav">
     <jdoc:include type="modules" name="topMenu" />
    </div>
	<?php endif; ?>
  </div>
  <div class="home_wrapper">
    <jdoc:include type="message" /> 
	<jdoc:include type="component" />
  </div>
  <div class="footer_wrapper">
    <jdoc:include type="modules" name="socialMedia" />
  </div>
  <div class="clearfix"></div>
</div>
</body>
</html>