-- MySQL dump 10.13  Distrib 5.1.70, for unknown-linux-gnu (x86_64)
--
-- Host: localhost    Database: camperhi_camper
-- ------------------------------------------------------
-- Server version	5.1.70-cll

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `jos_banner`
--

DROP TABLE IF EXISTS `jos_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_banner`
--

LOCK TABLES `jos_banner` WRITE;
/*!40000 ALTER TABLE `jos_banner` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_bannerclient`
--

DROP TABLE IF EXISTS `jos_bannerclient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_bannerclient`
--

LOCK TABLES `jos_bannerclient` WRITE;
/*!40000 ALTER TABLE `jos_bannerclient` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_bannerclient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_bannertrack`
--

DROP TABLE IF EXISTS `jos_bannertrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_bannertrack`
--

LOCK TABLES `jos_bannertrack` WRITE;
/*!40000 ALTER TABLE `jos_bannertrack` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_bannertrack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_categories`
--

DROP TABLE IF EXISTS `jos_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_categories`
--

LOCK TABLES `jos_categories` WRITE;
/*!40000 ALTER TABLE `jos_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_chrono_contact`
--

DROP TABLE IF EXISTS `jos_chrono_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_chrono_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `html` longtext NOT NULL,
  `scriptcode` longtext NOT NULL,
  `stylecode` longtext NOT NULL,
  `redirecturl` text NOT NULL,
  `emailresults` tinyint(1) NOT NULL,
  `fieldsnames` text NOT NULL,
  `fieldstypes` text NOT NULL,
  `onsubmitcode` longtext NOT NULL,
  `onsubmitcodeb4` longtext NOT NULL,
  `server_validation` longtext NOT NULL,
  `attformtag` longtext NOT NULL,
  `submiturl` text NOT NULL,
  `emailtemplate` longtext NOT NULL,
  `useremailtemplate` longtext NOT NULL,
  `paramsall` longtext NOT NULL,
  `titlesall` longtext NOT NULL,
  `extravalrules` longtext NOT NULL,
  `dbclasses` longtext NOT NULL,
  `autogenerated` longtext NOT NULL,
  `chronocode` longtext NOT NULL,
  `theme` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_chrono_contact`
--

LOCK TABLES `jos_chrono_contact` WRITE;
/*!40000 ALTER TABLE `jos_chrono_contact` DISABLE KEYS */;
INSERT INTO `jos_chrono_contact` VALUES (1,'contactUs','<h1>Contact Us</h1>\r\n<h2>Phone: 1300 930 803</h2>\r\n<div class=\"quote_wrapper\">\r\n          <ul>\r\n            <li>Enter your Name:</li>\r\n            <li>\r\n              <input name=\"entername\" type=\"text\" class=\"textarea\" value=\"\" />\r\n            </li>\r\n            <li>Enter your email address:</li>\r\n            <li>\r\n              <input name=\"emailaddress\" type=\"text\" class=\"textarea\" value=\"\" />\r\n            </li>\r\n            <li>Enter your phone number:</li>\r\n            <li>\r\n              <input name=\"phonenumber\" type=\"text\" class=\"textarea\" value=\"\" />\r\n            </li>\r\n            <li>Enter Your Message:</li>\r\n            <li>\r\n              <textarea name=\"message\" cols=\"30\" rows=\"3\" class=\"cf_inputbox\"></textarea>\r\n            </li>\r\n            <li></li>\r\n            <li>\r\n              <input type=\"submit\" name=\"submit\" id=\"button\" value=\"Submit\" class=\"button\" />\r\n            </li>\r\n      </ul>\r\n    </div>','','','thank.html',2,'entername,emailaddress,phonenumber,message','text,text,text,textarea','','','','','','','','formmethod=post\nLoadFiles=No\nsubmissions_limit=\nsubmissions_limit_error=Sorry but you can not submit the form again very soon like this!\nhandlepostedarrays=Yes\ndebug=0\ncheckToken=0\nmysql_type=1\nenmambots=No\ncaptcha_dataload=0\ncaptcha_dataload_skip=\nuseCurrent=\ndatefieldformat=d/m/Y\ndatefieldsnames=\ndatefieldextras=classes: [\'dashboard\']\ndbconnection=No\nsavedataorder=after_email\ndvfields=recordtime\ndvrecord=Record #n\nuploads=No\nuploadfields=\nuploadpath=\nfilename_format=$filename = date(\'YmdHis\').\'_\'.$chronofile[\'name\'];\nupload_exceedslimit=Sorry, Your uploaded file size exceeds the allowed limit.\nupload_lesslimit=Sorry, Your uploaded file size is less than the allowed limit\nupload_notallowed=Sorry, Your uploaded file type is not allowed\nimagever=No\nimtype=0\nimgver_error_msg=You have entered an incorrect verification code at the bottom of the form.\nvalidate=Yes\nvalidatetype=mootools\nvalidate_onlyOnBlur=1\nvalidate_wait=\nvalidate_onlyOnSubmit=1\nvalidation_type=default\nval_required=entername,emailaddress,phonenumber,message\nval_validate_number=\nval_validate_digits=\nval_validate_alpha=\nval_validate_alphanum=\nval_validate_date=\nval_validate_email=emailaddress\nval_validate_url=\nval_validate_date_au=\nval_validate_currency_dollar=\nval_validate_selection=\nval_validate_one_required=\nval_validate_confirmation=\nservervalidate=No\nautogenerated_order=3\nonsubmitcode_order=2\nplugins_order=1\nplugins=\nmplugins_order=\ntablenames=','','','','','','default',1,'','','','','');
/*!40000 ALTER TABLE `jos_chrono_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_chrono_contact_elements`
--

DROP TABLE IF EXISTS `jos_chrono_contact_elements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_chrono_contact_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `placeholder` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `code` longtext NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_chrono_contact_elements`
--

LOCK TABLES `jos_chrono_contact_elements` WRITE;
/*!40000 ALTER TABLE `jos_chrono_contact_elements` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_chrono_contact_elements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_chrono_contact_emails`
--

DROP TABLE IF EXISTS `jos_chrono_contact_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_chrono_contact_emails` (
  `emailid` int(11) NOT NULL AUTO_INCREMENT,
  `formid` int(11) NOT NULL,
  `to` text NOT NULL,
  `dto` text NOT NULL,
  `subject` text NOT NULL,
  `dsubject` text NOT NULL,
  `cc` text NOT NULL,
  `dcc` text NOT NULL,
  `bcc` text NOT NULL,
  `dbcc` text NOT NULL,
  `fromname` text NOT NULL,
  `dfromname` text NOT NULL,
  `fromemail` text NOT NULL,
  `dfromemail` text NOT NULL,
  `replytoname` text NOT NULL,
  `dreplytoname` text NOT NULL,
  `replytoemail` text NOT NULL,
  `dreplytoemail` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `params` longtext NOT NULL,
  `template` longtext NOT NULL,
  PRIMARY KEY (`emailid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_chrono_contact_emails`
--

LOCK TABLES `jos_chrono_contact_emails` WRITE;
/*!40000 ALTER TABLE `jos_chrono_contact_emails` DISABLE KEYS */;
INSERT INTO `jos_chrono_contact_emails` VALUES (1,1,'charles@onlinestart.com','','Contact us FIEC Mobile Version','','','','','','','entername','','emailaddress','','','','',1,'recordip=1\nemailtype=html\nenabled=1\neditor=1\nenable_attachments=1','<h1>Contact Us</h1>\r\n<h2>Phone: 1300 930 803</h2>\r\n<div class=\"quote_wrapper\">\r\n          <ul>\r\n            <li>Enter your Name:</li>\r\n            <li>\r\n              {entername}\r\n            </li>\r\n            <li>Enter your email address:</li>\r\n            <li>\r\n              {emailaddress}\r\n            </li>\r\n            <li>Enter your phone number:</li>\r\n            <li>\r\n              {phonenumber}\r\n            </li>\r\n            <li>Enter Your Message:</li>\r\n            <li>\r\n              {message}\r\n            </li>\r\n            <li></li>\r\n            <li>\r\n              \r\n            </li>\r\n      </ul>\r\n    </div>');
/*!40000 ALTER TABLE `jos_chrono_contact_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_chrono_contact_plugins`
--

DROP TABLE IF EXISTS `jos_chrono_contact_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_chrono_contact_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `event` varchar(255) NOT NULL,
  `params` longtext NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  `extra6` longtext NOT NULL,
  `extra7` longtext NOT NULL,
  `extra8` longtext NOT NULL,
  `extra9` longtext NOT NULL,
  `extra10` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_chrono_contact_plugins`
--

LOCK TABLES `jos_chrono_contact_plugins` WRITE;
/*!40000 ALTER TABLE `jos_chrono_contact_plugins` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_chrono_contact_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_components`
--

DROP TABLE IF EXISTS `jos_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_components`
--

LOCK TABLES `jos_components` WRITE;
/*!40000 ALTER TABLE `jos_components` DISABLE KEYS */;
INSERT INTO `jos_components` VALUES (1,'Banners','',0,0,'','Banner Management','com_banners',0,'js/ThemeOffice/component.png',0,'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n',1),(2,'Banners','',0,1,'option=com_banners','Active Banners','com_banners',1,'js/ThemeOffice/edit.png',0,'',1),(3,'Clients','',0,1,'option=com_banners&c=client','Manage Clients','com_banners',2,'js/ThemeOffice/categories.png',0,'',1),(4,'Web Links','option=com_weblinks',0,0,'','Manage Weblinks','com_weblinks',0,'js/ThemeOffice/component.png',0,'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n',1),(5,'Links','',0,4,'option=com_weblinks','View existing weblinks','com_weblinks',1,'js/ThemeOffice/edit.png',0,'',1),(6,'Categories','',0,4,'option=com_categories&section=com_weblinks','Manage weblink categories','',2,'js/ThemeOffice/categories.png',0,'',1),(7,'Contacts','option=com_contact',0,0,'','Edit contact details','com_contact',0,'js/ThemeOffice/component.png',1,'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n',1),(8,'Contacts','',0,7,'option=com_contact','Edit contact details','com_contact',0,'js/ThemeOffice/edit.png',1,'',1),(9,'Categories','',0,7,'option=com_categories&section=com_contact_details','Manage contact categories','',2,'js/ThemeOffice/categories.png',1,'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n',1),(10,'Polls','option=com_poll',0,0,'option=com_poll','Manage Polls','com_poll',0,'js/ThemeOffice/component.png',0,'',1),(11,'News Feeds','option=com_newsfeeds',0,0,'','News Feeds Management','com_newsfeeds',0,'js/ThemeOffice/component.png',0,'',1),(12,'Feeds','',0,11,'option=com_newsfeeds','Manage News Feeds','com_newsfeeds',1,'js/ThemeOffice/edit.png',0,'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n',1),(13,'Categories','',0,11,'option=com_categories&section=com_newsfeeds','Manage Categories','',2,'js/ThemeOffice/categories.png',0,'',1),(14,'User','option=com_user',0,0,'','','com_user',0,'',1,'',1),(15,'Search','option=com_search',0,0,'option=com_search','Search Statistics','com_search',0,'js/ThemeOffice/component.png',1,'enabled=0\n\n',1),(16,'Categories','',0,1,'option=com_categories&section=com_banner','Categories','',3,'',1,'',1),(17,'Wrapper','option=com_wrapper',0,0,'','Wrapper','com_wrapper',0,'',1,'',1),(18,'Mail To','',0,0,'','','com_mailto',0,'',1,'',1),(19,'Media Manager','',0,0,'option=com_media','Media Manager','com_media',0,'',1,'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,php,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=0\nallowed_media_usergroup=3\ncheck_mime=0\nimage_extensions=bmp,gif,jpg,php\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/php,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=0\n\n',1),(20,'Articles','option=com_content',0,0,'','','com_content',0,'',1,'show_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=0\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=0\nfeed_summary=0\nfilter_tags=\nfilter_attritbutes=\n\n',1),(21,'Configuration Manager','',0,0,'','Configuration','com_config',0,'',1,'',1),(22,'Installation Manager','',0,0,'','Installer','com_installer',0,'',1,'',1),(23,'Language Manager','',0,0,'','Languages','com_languages',0,'',1,'',1),(24,'Mass mail','',0,0,'','Mass Mail','com_massmail',0,'',1,'mailSubjectPrefix=\nmailBodySuffix=\n\n',1),(25,'Menu Editor','',0,0,'','Menu Editor','com_menus',0,'',1,'',1),(27,'Messaging','',0,0,'','Messages','com_messages',0,'',1,'',1),(28,'Modules Manager','',0,0,'','Modules','com_modules',0,'',1,'',1),(29,'Plugin Manager','',0,0,'','Plugins','com_plugins',0,'',1,'',1),(30,'Template Manager','',0,0,'','Templates','com_templates',0,'',1,'',1),(31,'User Manager','',0,0,'','Users','com_users',0,'',1,'allowUserRegistration=1\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n',1),(32,'Cache Manager','',0,0,'','Cache','com_cache',0,'',1,'',1),(33,'Control Panel','',0,0,'','Control Panel','com_cpanel',0,'',1,'',1),(34,'Chrono Forms','option=com_chronocontact',0,0,'option=com_chronocontact','Chrono Forms','com_chronocontact',0,'components/com_chronocontact/CF.png',0,'',1),(35,'Forms Management','',0,34,'option=com_chronocontact&act=all','Forms Management','com_chronocontact',0,'js/ThemeOffice/component.png',0,'',1),(36,'Form Wizard','',0,34,'option=com_chronocontact&task=form_wizard','Form Wizard','com_chronocontact',1,'js/ThemeOffice/component.png',0,'',1),(37,'Wizard Custom Elements','',0,34,'option=com_chronocontact&task=wizard_elements','Wizard Custom Elements','com_chronocontact',2,'js/ThemeOffice/component.png',0,'',1),(38,'Menu Creator','',0,34,'option=com_chronocontact&task=menu_creator','Menu Creator','com_chronocontact',3,'js/ThemeOffice/component.png',0,'',1),(39,'Menu Remover','',0,34,'option=com_chronocontact&task=menu_remover','Menu Remover','com_chronocontact',4,'js/ThemeOffice/component.png',0,'',1),(40,'Upgrade SQL and Load Demo Form','',0,34,'option=com_chronocontact&task=doupgrade','Upgrade SQL and Load Demo Form','com_chronocontact',5,'js/ThemeOffice/component.png',0,'',1),(41,'Validate Installation','',0,34,'option=com_chronocontact&task=validatelicense','Validate Installation','com_chronocontact',6,'js/ThemeOffice/component.png',0,'',1);
/*!40000 ALTER TABLE `jos_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_contact_details`
--

DROP TABLE IF EXISTS `jos_contact_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_contact_details`
--

LOCK TABLES `jos_contact_details` WRITE;
/*!40000 ALTER TABLE `jos_contact_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_contact_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_content`
--

DROP TABLE IF EXISTS `jos_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_content`
--

LOCK TABLES `jos_content` WRITE;
/*!40000 ALTER TABLE `jos_content` DISABLE KEYS */;
INSERT INTO `jos_content` VALUES (1,'Welcome','welcome','','<div><img src=\"images/stories/campre/image0.jpg\" border=\"0\" width=\"587\" height=\"306\" /></div>\r\n<h2>AWESOME CAMPERVANS | BUDGET CAMPERVANS</h2>\r\n<h1>CHEAP AUSTRALIAN CAMPERVANS FOR HIRE</h1>\r\n<p>Awesome Campervans is for the backpackers and budget travellers looking for a great way to travel Australia. There is no cheaper way to travel Australia than in one of our Awesome Campervans. All our Campervans come with a comfy bed, kitchen &amp; loads of Storage space.</p>\r\n<p>We have great deals for you to enjoy on your holidays and discounts on long term Rentals, were not just affordable we also have safe and reliable quality campervans.</p>\r\n<p>All Awesome Campervans are designed &amp; custom built by us, to offer total comfort for your travels.</p>\r\n<p>Our Campervans come fully serviced by our trusted Mechanics &amp; Automotive Professionals.</p>\r\n<p>You won\'t find a better value for money when renting our Campervans, our rental rates are the most competitive in the market.</p>','',1,0,0,0,'2012-03-20 07:22:23',62,'','2012-03-20 09:12:39',62,0,'0000-00-00 00:00:00','2012-03-20 07:22:23','0000-00-00 00:00:00','','','show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',3,0,13,'','',0,52,'robots=\nauthor='),(2,'campervans','campervans','','<div class=\"campervan_block\">\r\n<ul>\r\n<li><a href=\"index.php?Itemid=4\"><img src=\"images/stories/campre/car1.png\" border=\"0\" width=\"166\" height=\"79\" /><br /> 2 Person Classic Campervan</a></li>\r\n<li><a href=\"index.php?Itemid=5\"><img src=\"images/stories/campre/car2.png\" border=\"0\" width=\"166\" height=\"79\" /><br /> 2 Person Deluxe Campervan</a></li>\r\n<li><a href=\"index.php?Itemid=6\"><img src=\"images/stories/campre/car3.png\" border=\"0\" width=\"158\" height=\"80\" /><br /> 2 Person Super Deluxe Campervan</a></li>\r\n<li><a href=\"index.php?Itemid=7\"><img src=\"images/stories/campre/car4.png\" border=\"0\" width=\"158\" height=\"80\" /><br /> 3 Person Classic Campervan</a></li>\r\n<li><a href=\"index.php?Itemid=8\"><img src=\"images/stories/campre/car5.png\" border=\"0\" width=\"192\" height=\"86\" /><br /> 3 Person Super Deluxe Campervan</a></li>\r\n<li><a href=\"index.php?Itemid=9\"><img src=\"images/stories/campre/car6.png\" border=\"0\" width=\"189\" height=\"97\" /><br /> 4 Person Super Deluxe Campervan</a></li>\r\n</ul>\r\n</div>','',1,0,0,0,'2012-03-20 07:39:26',62,'','2012-03-20 09:46:55',62,0,'0000-00-00 00:00:00','2012-03-20 07:39:26','0000-00-00 00:00:00','','','show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',3,0,12,'','',0,325,'robots=\nauthor='),(3,'Insurances','insurances','','<p>Text Coming soon...</p>','',1,0,0,0,'2012-03-20 07:51:58',62,'','2012-03-20 09:03:28',62,0,'0000-00-00 00:00:00','2012-03-20 07:51:58','0000-00-00 00:00:00','','','show_title=1\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',4,0,11,'','',0,131,'robots=\nauthor='),(4,'Awesome 2 Person Classic Campervan','awesome-2-person-classic-campervan','','<p><img src=\"images/stories/campre/image00222 copy.jpg\" border=\"0\" width=\"300\" height=\"209\" /></p>\r\n<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Power Steering</li>\r\n<li>AM/FM Radio</li>\r\n<li>Your Choice of Automatic and Manuals Transmission</li>\r\n<li>Tinted Windows</li>\r\n<li>Flyscreens</li>\r\n<li>All Campers are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner, Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides Rest Area Guide, Regional Information Guides.</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Full Size Double Mattress.</li>\r\n<li>2x Pillows</li>\r\n<li>1x Sheet Set</li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge</li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs</li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler</li>\r\n<li>1 x Fry Pan</li>\r\n<li>1 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>Single Gas Cooker</li>\r\n<li>1 x Esky</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<p>Our Campervans come fully serviced by our trusted Mechanics &amp; Automotive Professionals.</p>\r\n<p>You won\'t find a better value for money when renting our Campervans, our rental rates are the most competitive in the market.</p>','',1,0,0,0,'2012-03-20 08:49:44',62,'','2012-03-20 09:02:30',62,0,'0000-00-00 00:00:00','2012-03-20 08:49:44','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',2,0,10,'','',0,113,'robots=\nauthor='),(5,'Awesome 2 Person Deluxe Campervan','awesome-2-person-deluxe-campervan','','<div><img src=\"images/stories/campre/joker222.jpg\" border=\"0\" width=\"223\" height=\"127\" /></div>\r\n<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Automatic with electronic overdrive for better fuel consumption</li>\r\n<li>Power Steering</li>\r\n<li>Electronic Windows</li>\r\n<li>Electronic Mirrors</li>\r\n<li>CD/Ipod/Mp3 connections</li>\r\n<li>Tinted Windows</li>\r\n<li>Sunroof</li>\r\n<li>All campervans are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 Hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner,           Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides           Rest Area Guide, Regional Information Guides.</li>\r\n<li><strong>FREE Electronic Safe Hire</strong> - Lock your valuable away</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Double Mattress</li>\r\n<li>2x Pillows </li>\r\n<li>1x Sheet Set </li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge </li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs </li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler </li>\r\n<li>1 x Fry Pan</li>\r\n<li>1 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>Single Gas Cooker</li>\r\n<li>1 x Esky</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Specifications:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>1998 Toyota Estima</li>\r\n<li>Engine - 2.4L</li>\r\n<li>Fuel Consumption - 10 - 12 Litres per 100km</li>\r\n<li>Air-Conditioning - Yes</li>\r\n<li>Electric Windows - Yes</li>\r\n<li>Power steering - Yes</li>\r\n<li>Cup Holders - Yes</li>\r\n<li>CD Player - Yes</li>\r\n<li>Bed to Roof height - 72cm</li>\r\n<li>Bed length - 1.8m</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Dimensions:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Length - 4.6m</li>\r\n<li>Width - 1.65m</li>\r\n<li>External height - 1.8</li>\r\n<li>Internal height - 1.26m </li>\r\n<li>Bed to roof height - 72cm</li>\r\n</ul>\r\n</div>\r\n<p><img src=\"images/stories/campre/layout.png\" border=\"0\" width=\"250\" height=\"322\" /></p>','',1,0,0,0,'2012-03-20 09:19:40',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 09:19:40','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,9,'','',0,123,'robots=\nauthor='),(6,'Awesome 2 Person Super Deluxe Campervan','awesome-2-person-super-deluxe-campervan','','<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Automatic with electronic overdrive for better fuel consumption</li>\r\n<li>Power Steering</li>\r\n<li>Electronic Windows</li>\r\n<li>Electronic Mirrors</li>\r\n<li>CD/Ipod/Mp3 connections</li>\r\n<li>Tinted Windows</li>\r\n<li>Sunroof</li>\r\n<li>All campervans are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 Hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n<li>Fridge</li>\r\n<li>240 Volt Power Outlet</li>\r\n<li>Caravan park Adapter with a 15 AMP Extension Lead</li>\r\n<li>Plenty of storage underneath the seats</li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner, Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides Rest Area Guide, Regional Information Guides.</li>\r\n<li><strong>FREE</strong> Electronic Safe Hire - Lock your valuable away</li>\r\n<li><strong>FREE</strong> Fridge</li>\r\n<li><strong>FREE</strong> Kettle</li>\r\n<li><strong>FREE </strong>Toaster</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Double  Mattress</li>\r\n<li>2x Pillows</li>\r\n<li>1x Sheet Set</li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge</li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs</li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler</li>\r\n<li>1 x Fry Pan</li>\r\n<li>1 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>1 x toaster</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>Single Gas Cooker</li>\r\n<li>1 x Fridge</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Specifications:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>1998 Toyota Estima</li>\r\n<li>Engine - 2.4L</li>\r\n<li>Fuel Consumption - 10 - 12 Litres per 100km</li>\r\n<li>Air-Conditioning - Yes</li>\r\n<li>Electric Windows - Yes</li>\r\n<li>Power steering - Yes</li>\r\n<li>Cup Holders - Yes</li>\r\n<li>CD Player - Yes</li>\r\n<li>Bed to Roof height - 72cm</li>\r\n<li>Bed length - 1.8m</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Dimensions:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Length - 4.6m</li>\r\n<li>Width - 1.65m</li>\r\n<li>External height - 1.8</li>\r\n<li>Internal height - 1.26m</li>\r\n<li>Bed to roof height - 72cm</li>\r\n</ul>\r\n</div>\r\n<p><img src=\"images/stories/campre/layout1.jpg\" border=\"0\" width=\"250\" height=\"322\" /></p>','',1,0,0,0,'2012-03-20 09:23:57',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 09:23:57','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,8,'','',0,82,'robots=\nauthor='),(7,'3 Person Classic Campervan','3-person-classic-campervan','','<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Power Steering</li>\r\n<li>AM/FM Radio</li>\r\n<li>Manuals Transmission</li>\r\n<li>Tinted Windows</li>\r\n<li>Flyscreens</li>\r\n<li>2 Person Tent</li>\r\n<li>All Campers are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner, Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides Rest Area Guide, Regional Information Guides.</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Full Size Double Mattress.</li>\r\n<li>2x Pillows</li>\r\n<li>1x Sheet Set</li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge</li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs </li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler </li>\r\n<li>1 x Fry Pan</li>\r\n<li>1 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>1 x Gas Cooker</li>\r\n<li>Fridge</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Specifications:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>1998 Toyota Estima</li>\r\n<li>Engine - 2.4L</li>\r\n<li>Fuel Consumption - 10 - 12 Litres per 100km</li>\r\n<li>Air-Conditioning - Yes</li>\r\n<li>Electric Windows - Yes</li>\r\n<li>Power steering - Yes</li>\r\n<li>Cup Holders - Yes</li>\r\n<li>CD Player - Yes</li>\r\n<li>Bed to Roof height - 72cm</li>\r\n<li>Bed length - 1.8m</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Dimensions:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Length - 4.6m</li>\r\n<li>Width - 1.65m</li>\r\n<li>External height - 1.8</li>\r\n<li>Internal height - 1.26m</li>\r\n<li>Bed to roof height - 72cm</li>\r\n</ul>\r\n</div>\r\n<p><img src=\"images/stories/campre/layout1.jpg\" border=\"0\" width=\"250\" height=\"322\" /></p>','',1,0,0,0,'2012-03-20 09:31:53',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 09:31:53','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,7,'','',0,84,'robots=\nauthor='),(8,'3 Seater Super Deluxe Campervan','3-seater-super-deluxe-campervan','','<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Automatic with electronic overdrive for better fuel consumption</li>\r\n<li>Power Steering</li>\r\n<li>Electronic Windows</li>\r\n<li>Electronic Mirrors</li>\r\n<li>CD/Ipod/Mp3 connections</li>\r\n<li>Tinted Windows</li>\r\n<li>Sunroof</li>\r\n<li>All campervans are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 Hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n<li>Fridge/freezer</li>\r\n<li>240 Volt Power Outlet</li>\r\n<li>Caravan park Adapter with a 15 AMP Extension Lead</li>\r\n<li>Sits 3 People</li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner, Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides Rest Area Guide, Regional Information Guides.</li>\r\n<li><strong>FREE</strong> Electronic Safe Hire - Lock your valuable away</li>\r\n<li><strong>FREE</strong> Fridge</li>\r\n<li><strong>FREE</strong> Kettle</li>\r\n<li><strong>FREE </strong>Toaster</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping (sleeps 2 people in the campervan &amp; 1 Person in a 2 Person Tent):</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Full Size Double Mattress.</li>\r\n<li>2x Pillows</li>\r\n<li>1x Sheet Set</li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge</li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs</li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler</li>\r\n<li>1 x Fry Pan</li>\r\n<li>1 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>1 x toaster</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>Single Gas Cooker</li>\r\n<li>1 x Fridge/freezer</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Specifications:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>1998 Toyota Estima</li>\r\n<li>Engine - 2.4L</li>\r\n<li>Fuel Consumption - 10 - 12 Litres per 100km</li>\r\n<li>Air-Conditioning - Yes</li>\r\n<li>Electric Windows - Yes</li>\r\n<li>Power steering - Yes</li>\r\n<li>Cup Holders - Yes</li>\r\n<li>CD Player - Yes</li>\r\n<li>Bed to Roof height - 72cm</li>\r\n<li>Bed length - 1.8m</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Dimensions:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Length - 4.6m</li>\r\n<li>Width - 1.65m</li>\r\n<li>External height - 1.8</li>\r\n<li>Internal height - 1.26m</li>\r\n<li>Bed to roof height - 72cm</li>\r\n</ul>\r\n</div>\r\n<p><img src=\"images/stories/campre/layout1.jpg\" border=\"0\" width=\"250\" height=\"322\" /></p>','',1,0,0,0,'2012-03-20 09:39:28',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 09:39:28','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,6,'','',0,98,'robots=\nauthor='),(9,'4 Person Super Deluxe Campervan with Roof Top Tent','4-person-super-deluxe-campervan-with-roof-top-tent','','<div><img src=\"images/stories/campre/cover.jpg\" border=\"0\" width=\"500\" height=\"188\" /></div>\r\n<h3>All Campervans Come With:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Air Conditioning</li>\r\n<li>Automatic with electronic overdrive for better fuel consumption</li>\r\n<li>Power Steering</li>\r\n<li>Electronic Windows</li>\r\n<li>Electronic Mirrors</li>\r\n<li>CD/Ipod/Mp3 connections</li>\r\n<li>Tinted Windows</li>\r\n<li>Sunroof</li>\r\n<li>All campervans are fitted with Fire Extinguishers</li>\r\n<li>Premium 24 Hour NRMA Roadside Assistance</li>\r\n<li>All Campers are fitted with E-Tags for Toll-way travel<span class=\"red\">*</span></li>\r\n<li>Fridge/freezer</li>\r\n<li>240 Volt Power Outlet</li>\r\n<li>Caravan park Adapter with a 15 AMP Extension Lead</li>\r\n<li>Sits 4 People</li>\r\n</ul>\r\n</div>\r\n<h3>Whats Included in a Campervan:</h3>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li><strong>FREE \'Welcome Pack\'</strong> - Soap, Mini Shampoo &amp; Conditioner, Tea, Coffee, Sugar, Salt &amp; Pepper.</li>\r\n<li><strong>FREE \'Travel Pack\' </strong>- Camping Grounds Location Guides Rest Area Guide, Regional Information Guides</li>\r\n<li><strong>FREE</strong> Electronic Safe Hire - Lock your valuable away</li>\r\n<li><strong>FREE</strong> Fridge</li>\r\n<li><strong>FREE</strong> Kettle</li>\r\n<li><strong>FREE </strong>Toaster</li>\r\n</ul>\r\n</div>\r\n<h4>Sleeping (sleeps 2 people in the campervan &amp; 1 Person in a 2 Person Tent):</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Full Size Double Mattress.</li>\r\n<li>2x Pillows</li>\r\n<li>1x Sheet Set</li>\r\n<li>1x Duvet</li>\r\n</ul>\r\n</div>\r\n<h4>Cooking &amp; Living:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>2x Bath Towels</li>\r\n<li>Dishwashing Liquid &amp; Sponge</li>\r\n<li>Tea Towels</li>\r\n<li>Dinner Plates, Bowls</li>\r\n<li>Cups, Coffee Mugs</li>\r\n<li>Set of Cutlery, Tongs</li>\r\n<li>Mixing Bowls</li>\r\n<li>Egg Rings / Spatula</li>\r\n<li>Chopping Board</li>\r\n<li>Can Opener, Vegetable Peeler</li>\r\n<li>1 x Fry Pan</li>\r\n<li>2 x Saucepans</li>\r\n<li>1 x Kettle</li>\r\n<li>1 x toaster</li>\r\n<li>Dust pan &amp; Broom</li>\r\n<li>1 Gas Cooker</li>\r\n<li>1 x Fridge/freezer</li>\r\n<li>2 x Folding Picnic Chairs</li>\r\n<li>1 x Folding Table</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Specifications:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>1998 Toyota Estima</li>\r\n<li>Engine - 2.4L</li>\r\n<li>Fuel Consumption - 10 - 12 Litres per 100km</li>\r\n<li>Air-Conditioning - Yes</li>\r\n<li>Electric Windows - Yes</li>\r\n<li>Power steering - Yes</li>\r\n<li>Cup Holders - Yes</li>\r\n<li>CD Player - Yes</li>\r\n<li>Bed to Roof height - 72cm</li>\r\n<li>Bed length - 1.8m</li>\r\n</ul>\r\n</div>\r\n<h4>Vechicle Dimensions:</h4>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Length - 4.6m</li>\r\n<li>Width - 1.65m</li>\r\n<li>External height - 1.8</li>\r\n<li>Internal height - 1.26m</li>\r\n<li>Bed to roof height - 72cm</li>\r\n</ul>\r\n</div>','',1,0,0,0,'2012-03-20 09:42:49',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 09:42:49','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,5,'','',0,87,'robots=\nauthor='),(10,'Location Info','location-info','','<ul id=\"accordion\">\r\n<li> <a class=\"item popular\" href=\"#\" rel=\"popular\">Sydney NSW Office</a> \r\n<ul style=\"text-align: left;\">\r\nNetwork Rentals - Agent for Awesome Campers\r\n</ul>\r\n<ul>\r\n1/42 Garema Circuit Kingsgrove NSW 2208 \r\n</ul>\r\n<ul>\r\n<br /> \r\n</ul>\r\n<ul>\r\nMonday - Friday: 9:00am – 3:00pm \r\n</ul>\r\n<ul>\r\nSaturday: 9:00am – 12:00pm \r\n</ul>\r\n<ul>\r\nSunday: Close \r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Cairns QLD Office</a> \r\n<ul>\r\nSun City Cars - Agent for Awesome campers \r\n</ul>\r\n<ul>\r\n147 Sheridan Street Cairns QLD 4870 \r\n</ul>\r\n<ul>\r\n<br /> \r\n</ul>\r\n<ul>\r\nMonday - Friday: 9:00am – 3:00pm \r\n</ul>\r\n<ul>\r\nSaturday: 9:00am - 12:00pm \r\n</ul>\r\n<ul>\r\nSunday: Closed \r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Brisbane QLD Office</a> \r\n<ul>\r\nAwesome Campers \r\n</ul>\r\n<ul>\r\n269 Earnshaw Road(Entrance via Axford St) Northgate QLD 4013 \r\n</ul>\r\n<ul>\r\n<br /> \r\n</ul>\r\n<ul>\r\nMonday - Friday: 9:00am – 3:00pm \r\n</ul>\r\n<ul>\r\nSaturday: 9:00am – 1:00pm \r\n</ul>\r\n<ul>\r\nSunday: Closed \r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Melbourne VIC Office</a> \r\n<ul>\r\nCalypso Campervans - Agent for Awesome Campers \r\n</ul>\r\n<ul>\r\n45 Bond Street Ringwood VIC 3134 \r\n</ul>\r\n<ul>\r\n<br /> \r\n</ul>\r\n<ul>\r\nMonday – Friday: 9:00am – 3:00pm \r\n</ul>\r\n<ul>\r\nSaturday: 9:00am - 12:00pm \r\n</ul>\r\n<ul>\r\nSunday: Closed \r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Adelaide SA Office</a> \r\n<ul>\r\nAcacia Car Rentals - Agent for Awesome Campers \r\n</ul>\r\n<ul>\r\n91 Sir Donald Bradman Drive Hilton SA 5033 \r\n</ul>\r\n<ul>\r\nMonday – Friday: 8:30am – 3:00pm \r\n</ul>\r\n<ul>\r\nSaturday: 8:30am - 12:00pm \r\n</ul>\r\n<ul>\r\nSunday: Closed \r\n</ul>\r\n</li>\r\n</ul>\r\n<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\" type=\"text/javascript\"></script>\r\n<script src=\"js/jquery.easing.1.3.js\" type=\"text/javascript\"></script>\r\n<script type=\"text/javascript\">// <![CDATA[\r\n	$(document).ready(function () {\r\n		$(\'#accordion a.item\').click(function () {\r\n			//slideup or hide all the Submenu\r\n			$(\'#accordion li\').children(\'ul\').slideUp(\'fast\');	\r\n			//remove all the \"Over\" class, so that the arrow reset to default\r\n			$(\'#accordion a.item\').each(function () {\r\n				if ($(this).attr(\'rel\')!=\'\') {\r\n					$(this).removeClass($(this).attr(\'rel\') + \'Over\');	\r\n				}\r\n			});\r\n			//show the selected submenu\r\n			$(this).siblings(\'ul\').slideDown(\'fast\');\r\n			//add \"Over\" class, so that the arrow pointing down\r\n			$(this).children(\'a\').addClass($(this).children(\'li a\').attr(\'rel\') + \'Over\');			\r\n			return false;\r\n		});\r\n	});\r\n// ]]></script>','',1,0,0,0,'2012-03-20 09:55:19',62,'','2013-02-11 07:02:51',62,0,'0000-00-00 00:00:00','2012-03-20 09:55:19','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',23,0,4,'','',0,1487,'robots=\nauthor='),(11,'Terms & Conditions','terms-a-conditions','','<h2>Awesome Campervan &amp; Van Hire Pty Ltd Terms &amp; Conditions</h2>\r\n<p>This rental agreement is made on the date specified in the Schedule   on the reverse side hereof (\"the Schedule\") between Awesome Van &amp;   Camper Hire (\"the owner\") and the customer (\"the hirer\") whose name and   address appears in the Schedule. The owner and hirer agree as follows:</p>\r\n<p class=\"blue_header\">(1) RATES AND CONDITIONS</p>\r\n<p>Rates and conditions quoted on our website and/or documentation are   subject to change without notice. However (subject to changes in   legislation or errors) we will not alter rates or conditions applicable   to your rental once your booking has been confirmed by Awesome Van &amp;   Camper Hire. Please note all prices are quoted and payable in   Australian Dollars. If the need should arise for a refund, it will be   applied in Australian Dollars. Awesome accepts no responsibility for   exchange rate fluctuations, positive or negative.</p>\r\n<p class=\"blue_header\">(2) DEFINITIONS</p>\r\n<p>“This Agreement” means the Rental Agreement and these Terms and   Conditions. “Customer” means the person or persons nominated as the   hirer and any person whose credit card is presented for payment of the   Customer’s charges. “Awesome” means Awesome Van &amp; Campervan Hire.</p>\r\n<p>“Rental Period” means the hire period or any agreed variation thereof   and any additional period during which the Vehicle is in the Customer’s   possession or control. “Vehicle” means the Vehicle hired by the   Customer and includes tyres, tools, accessories, camping utensils, and   all other equipment, documents, or additional hire items related to the   Vehicle and any replacement or substitute Vehicle that may be provided.</p>\r\n<p class=\"blue_header\">(3) BRANCH HOURS</p>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Kingsgrove, Sydney (NSW) Office</li>\r\n<li>Monday - Friday: 9:00am – 5:00pm</li>\r\n<li>Weekend: 9:00am – 2:00pm</li>\r\n<li>Cairns (QLD) Office</li>\r\n<li>Monday - Friday: 8:30am – 3:30pm</li>\r\n<li>Weekend: Closed</li>\r\n<li>Northgate, Brisbane (QLD) Office</li>\r\n<li>Monday - Friday: 9:00am – 5:00pm</li>\r\n<li>Weekend: 9:00am – 2:00pm</li>\r\n<li>Berwick, Melbourne (VIC) Office</li>\r\n<li>Monday – Friday: 8:30am – 3:30pm</li>\r\n<li>Weekend: Closed</li>\r\n<li>Adelaide (SA) Office</li>\r\n<li>Monday – Friday: 8:30am – 4:00pm</li>\r\n<li>Saturday 9:00am - 12:00pm</li>\r\n<li>Sunday: Closed</li>\r\n<li>All branches are closed Christmas Day (25 December), Boxing Day (26 December) and New Year\'s Day (1 January). Vehicles must be returned before 2pm Christmas Eve and New Year\'s Eve.</li>\r\n<li>State branches are also closed for their respective state public holidays.</li>\r\n</ul>\r\n</div>\r\n<p class=\"blue_header\">(4) RENTAL DURATION &amp; EXTENSIONS</p>\r\n<p>4.1 Rental days are calculated on a calendar day basis. When   calculating the number of days the Vehicle is rented, the day of pick-up   is counted as day one of the rental, regardless of pick-up time. The   day of the Vehicle’s return is counted as the final day of the rental   regardless of drop-off time.</p>\r\n<p>4.2 Late pick up or early return of the Vehicle does not entitle the Customer to any refund of the unused portion of the rental.</p>\r\n<p>4.3 Extensions may be organized through the sales office, subject to   availability, and at the current rental rate. The extra cost of an   extended rental must be paid by credit card on confirmation of the   rental extension.</p>\r\n<p>4.4 Failure to obtain an authorization for a rental extension will   result in a late fee of AU$100.00 per day in addition to the daily   rental rate for each day until the Vehicle is returned. The daily rental   rate charged will be the rate applicable on the day of extension per   Vehicle for the extended rental period.</p>\r\n<p class=\"blue_header\">(5) MINIMUM DAYS &amp; ONE WAY RENTALS</p>\r\n<p>5.1 Minimum rental period is 5 days, depending on pick up location &amp; availability.</p>\r\n<p>5.2 Minimum rental period for one-way rentals is 5 days, depending on   pick up location. One way rentals are available between all branch   locations.</p>\r\n<p>5.3 Minimum rental periods are subject to change, and any such change will be notified to you prior to booking confirmation.</p>\r\n<p class=\"blue_header\">(6) VEHICLE PICK-UP AND DROP-OFF</p>\r\n<p>6.1 Vehicles are available for pick-up at a designated time on the   first day of the hire period and must be returned on the agreed date at   the nominated depot before 2:00pm.</p>\r\n<p>6.2 Vehicle pick-up or return outside of branch hours are subject to an after-hours fee of AU$100.</p>\r\n<p>6.3 The Customer acknowledges having received the Vehicle in a clean   condition, with a full fuel tank and with linen &amp; kitchen utensil   clean and tidy.</p>\r\n<p>6.4 The Customer will return the Vehicle in a clean condition with   exterior washed, kitchen utensils and linen in a clean condition and a   full fuel tank on the return date, time and location set out in the   Rental Agreement.</p>\r\n<p>6.5 The Customer acknowledges that Awesome will reasonably determine   what, if any, refund may be warranted if the Vehicle is returned or the   Customer ceases to have the use of the Vehicle prior to the agreed   return date.</p>\r\n<p class=\"blue_header\">(7) CHANGE OF DROP-OFF LOCATION</p>\r\n<p>7.1 If the Customer wishes to change the drop-off destination, they   must first obtain authorization from the office by calling 1300 930 803.</p>\r\n<p>7.2 Subject to the change being approved an additional charge of up   to AU$1100 may apply, which will be notified to you at time of approval.</p>\r\n<p>The fee may apply in all cases irrespective of the reason for location change.</p>\r\n<p class=\"blue_header\">(8) EARLY &amp; LATE DROP-OFFS</p>\r\n<p>8.1 Early pick-ups can be organized in advance, subject to   availability. Late returns will be charged for additional days. If no   extension has been approved, we have the right to report the Vehicle as   stolen and press charges.</p>\r\n<p>8.2 Administration fees may also apply. If the Customer wishes to   drop-off the Vehicle after business hours, they must first obtain   approval from the destination branch.</p>\r\n<p class=\"blue_header\">(9) LICENCE</p>\r\n<p>9.1 Branch staff must view and record details of the Customer’s   Drivers Licence on collection of your Vehicle. Branch staff must view   and record passport details of International Licence holders.</p>\r\n<p>9.2 For Customers with an International Licence, the Licence needs to   have been valid for 6 months and the driver must be a least 21 years of   age. A valid overseas or International Drivers Licence is permitted.</p>\r\n<p class=\"blue_header\">(10) AGE RESTRICTIONS</p>\r\n<p>Drivers must be 21 years of age or over. A medical certificate   stating that the customer is fit to drive the Vehicle they have booked   for the duration of the hire is required for drivers over the age of 75.</p>\r\n<p class=\"blue_header\">(11) USE OF THE VEHICLE</p>\r\n<p>11.1 The Customer agrees that, during the Rental Period, the Customer will not allow the Vehicle to be;</p>\r\n<p>a) Driven in other than a prudent and cautious manner. A single   vehicle rollover is considered a breach of this condition and the   customer will be responsible for the cost of damage. For any damage due   to vehicle use in contravention of Clause 11 “Use of Vehicle”. Any   damage caused by willful misconduct (e.g. sitting or standing on the   bonnet or roof of the Vehicle or disregarding signposted height   restrictions entering or exiting Car Parks or Drive Thrus);</p>\r\n<p>b) Driven by a person under the influence of alcohol or drugs or with a blood alcohol level in excess of that permitted by law;</p>\r\n<p>c) Left with the ignition key in the Vehicle while it is unoccupied;</p>\r\n<p>d) Damaged by:</p>\r\n<p>(i) Submersion in water;</p>\r\n<p>(ii) Contact with salt water;</p>\r\n<p>(iii) Creek or river crossing;</p>\r\n<p>(iv) Driving through low plain flooded areas;</p>\r\n<p>(v) Beach driving</p>\r\n<p>e) Used for any illegal purpose or in any race, rally or contest;</p>\r\n<p>f) Used to tow any vehicle or trailer;</p>\r\n<p>h) Used to carry more persons than is permitted by any relevant   authority or detailed in the Vehicle Manual or on the Vehicle or   specified in this Agreement;</p>\r\n<p>i) Used to carry volatile liquids, gases, explosives or other corrosive or inflammable material.</p>\r\n<p>j) NOT to be driven on Country roads at SUNSET or AFTER Dark as the risk of COLLISION with Native Wildlife is Greatly increased.</p>\r\n<p>11.2 Road restrictions apply as follows:</p>\r\n<p>Vehicles must not be used on any unsealed road (being a road not   sealed with a hard material such as tar, bitumen or concrete). Off road   conditions include, but are not limited to: fire trails, beaches, sand,   tracks, fields or paddocks. The only exception to this is reasonable use   of access roads to recognized commercial campgrounds.</p>\r\n<p>11.3 We value your well-being, and for safety purposes Awesome   reserves the right, at its sole discretion, to restrict vehicle   movements in certain areas due to adverse road or weather conditions,   and the instance to nominated destinations in relation to the length of   hire period. Awesome will advise you on pick up of any travel   restrictions known at that time.</p>\r\n<p>11.4 Where Awesome mandates a change in drop off location, fees as per Clause 6 will not apply.</p>\r\n<p>11.5 The Customer shall not make any alternations or additions to the Vehicle without the prior written consent of Awesome.</p>\r\n<p>11.6 The Customer will not allow any animals to be carried in the Vehicle, excluding registered guide dogs.</p>\r\n<p>11.7 The Customer shall take all reasonable steps to properly   maintain the Vehicle, including daily checks of the oil, water and   batteries, and will contact Awesome immediately should vehicle warning   lights indicate any potential malfunction.</p>\r\n<p class=\"blue_header\">(12) MAINTENANCE AND REPAIRS</p>\r\n<p>12.1 Awesome will reimburse customers for expenditure up to AU$100   reasonably incurred in rectifying any mechanical failure to the drive   train and engine of the Vehicle. For repairs costing over AU$100,   Awesome will need to be informed and confirm the repair in advance.   Repairs will be approved and reimbursement, where applicable, will be   granted provided the Customer was not responsible for the damage. In all   cases, receipts must be submitted for any repair or the claim will not   be paid.</p>\r\n<p>12.2 Subject to the terms of the Excess Reduction, the Customer will   pay for the cost of repairing or replacing the tyres damaged during the   Rental Period except if the tyre is defective and is returned by the   Customer to Awesome for inspection and is subject to a warranty claim on   the manufacturer.</p>\r\n<p>12.3 The Customer will be liable for any cost associated with the   incorrect use of fuel (being diesel or petrol), this includes Bio-Diesel   which should NOT be used, or water or other contamination of fuel.</p>\r\n<p>12.4 The Customer will pay Awesome the daily rental rate for the period the Vehicle is off fleet for accident repairs.</p>\r\n<p class=\"blue_header\">(13) ROAD SIDE ASSISTANCE</p>\r\n<p>Any problems associated with the Vehicle, including equipment   failure, must be reported to Awesome within 24 hours in order to give   Awesome the opportunity to rectify the problem during the rental.   Failure to do so may compromise any claims for compensation. Awesome   reserves the right not to accept liability for any claims submitted   after this period.</p>\r\n<p>Please contact Awesome on: 1300 930 803.</p>\r\n<p class=\"blue_header\">(14) VEHICLE AVAILABILITY</p>\r\n<p>14.1 Vehicles cannot be requested by make or model, only by vehicle category.</p>\r\n<p>14.2 Awesome will endeavor to supply the vehicle category selected,   however should the Vehicle booked be unavailable through unforeseen   circumstances, Awesome reserve the right to substitute an alternative   vehicle without prior notification. The alternative Vehicle shall be as   close a substitute for the booked vehicle as possible. Awesome will   reasonably determine what, if any, refund may be warranted if a vehicle   substitution is required.</p>\r\n<p>14.3 Should the customer decide to take a lesser Vehicle than booked they will not be entitled to any refund.</p>\r\n<p class=\"blue_header\">(15) TITLE TO VEHICLE</p>\r\n<p>The Customer acknowledges that Awesome retains title to the Vehicle   at all times. The Customer shall not agree, attempt, offer or purport to   sell, assign, sub-let, lend, pledge, mortgage, let on hire or otherwise   part with or attempt to part with the personal possession of or   otherwise deal with the Vehicle.</p>\r\n<p class=\"blue_header\">((16) FOR YOUR PROTECTION</p>\r\n<p>Personal Injury is covered in most cases through Registration Third   Party Insurance. However, we strongly recommend that all people   travelling in Australia take out Personal Travel Insurance.</p>\r\n<p>Awesome does not accept any liability for personal injuries sustained during rental.</p>\r\n<p class=\"blue_header\">(17) VEHICLE DAMAGE - EXCESS REDUCTION OPTIONS</p>\r\n<p>17.1 The Customer understands that:</p>\r\n<p>a) The Vehicle is insured for third party vehicle and property damage;</p>\r\n<p>b) The Customer will have to pay an excess in respect of any damage incurred whilst in the Customer’s possession;</p>\r\n<p>c) The excess may be reduced by taking out Excess Reduction coverage (see below)</p>\r\n<p>17.2 Any Excess Reduction is void, and the Customer will be   responsible for the total cost of any damage (as per Clause 17.6) if the   Customer breaches any of the conditions of Clause 11.</p>\r\n<p>17.3 If no Excess Reduction Option is taken, the Customer is   responsible for the first AU$3500.00 of the cost of damage as described   in Clause 17.6</p>\r\n<p>EXCESS REDUCTION OPTIONS</p>\r\n<p>You are required to take one of the following Excess Reduction Options:</p>\r\n<p>17.4 The excess applies in respect of each claim, not rental.</p>\r\n<p>17.5 The excess is applicable regardless of who is at fault and must   be paid at the time the accident report is completed, not at the   completion of the Rental. The excess will be refunded only if Awesome is   successful in recovering the cost of the damages from the third party.   Please note that third party claims can take months or even years to   resolve.</p>\r\n<p>17.6 Damage includes any and all damage to third party property,   damage to the rented Vehicle including windscreens, tyres, towing and   recovery costs, theft, fire, break-in or vandalism. This also includes   the cost of the daily rental rate for the period the Vehicle is off   fleet for repair.</p>\r\n<p class=\"blue_header\">(18) LIABILITY C COVER</p>\r\n<p>Awesome Liability C Cover is a full cover option. Subject to Clauses   17 and 19, where Liability C cover is taken out, the Customer will pay   no excess for any damage to the Vehicle, including tyres, all   window/glass breakages (with the exception of sunroof), under body and   overhead damage.</p>\r\n<p>AWESOME STRONGLY RECOMMENDS OUR CUSTOMERS TO TAKE OUT LIABILITY COVER C FOR COMPLETE PEACE OF MIND.</p>\r\n<p>WE ALSO STRONGLY RECOMMEND THAT YOU DO NOT DRIVE ON COUNTRY ROADS AT   SUNSET OR AFTER DARK AS THE RISK OF COLLISION WITH NATIVE WILDLIFE IS   GREATLY INCREASED.</p>\r\n<p class=\"blue_header\">(19) EXCLUSIONS</p>\r\n<p>The Customer acknowledges that they are responsible for all costs for   the following damage irrespective of Excess Reduction options that may   have been taken. Damage as identified below is specifically excluded   from any Excess Reduction or Liability C Cover limitation of liability   and the Customer remains fully liable for all costs incurred:</p>\r\n<p>a) For any damage due to vehicle use in contravention of Clause 11 “Use of Vehicle”;</p>\r\n<p>b) Any damage caused by willful misconduct (e.g. sitting or standing   on the bonnet or roof of the Vehicle) and driving under the influence of   alcohol or drugs and negligence resulting in damage to the hired   Vehicle or third party vehicle/property;</p>\r\n<p>c) For any loss or damage to personal belongings: Awesome recommends   the Customer does not leave valuables in the Vehicle and that they take   out personal insurance;</p>\r\n<p>d) If the Customer is proven to have not abided by the current road   rules, resulting in damage to the hired Vehicle or third party   vehicle/property;</p>\r\n<p>e) The cost to retrieve or recover the Vehicle, which as the result of accident is un-drivable or which has become bogged;</p>\r\n<p>f) The cost to replace keys which have been lost or stolen, or retrieval of keys which have been locked in the Vehicle;</p>\r\n<p>g) For any costs relating to overhead or under body damage however caused (not excluded with Liability B Cover);</p>\r\n<p>h) Drivers not identified on the rental agreement and/or drivers that   have a Licence that has been cancelled or suspended and/or drivers who   have a Licence that is classified as a Learners (L) or Probationary (P)   Licence;</p>\r\n<p>i) Any damage to the sunroof;</p>\r\n<p>j) Any damage caused by driving while the Vehicle’s engine is overheated; and</p>\r\n<p>k) Any damage caused to the interior of the Vehicle.</p>\r\n<p>l) Any damage caused to the roof pod or roof top tent mounted upon the Vehicle.</p>\r\n<p class=\"blue_header\">(20) VEHICLE SECURITY DEPOSIT</p>\r\n<p>20.1 On pick-up of the Vehicle, the Customer agrees to pay a Vehicle   Security Deposit. The Customer authorizes Awesome to deduct from the   Security Deposit any amounts due by them to Awesome arising out of the   Agreement. The Vehicle Security Deposit amount is determined by the   Excess Reduction Option selected.</p>\r\n<p>20.2 If the Customer does not take the Liability A or Liability B or   Liability C options, the Security Deposit of AU$3500.00 is payable by   credit card or by cash.</p>\r\n<p>20.3 If the Liability A option has been taken the Security Deposit of AU$2500.00 is payable by credit card or by cash.</p>\r\n<p>20.4 If the Liability B option has been taken, a Security Deposit of AU$1500.00 is payable by credit card or by cash.</p>\r\n<p>20.5 The Security Deposit is fully refundable provided the Vehicle is   returned on time, to the correct location, undamaged, with a clean   exterior, with a clean interior, and with a full fuel tank (vehicle   petrol).</p>\r\n<p>20.6 Awesome reserves the right to retain a AU$150 soiling fee if the   Vehicle is not returned with the interior and exterior in a clean   condition and free of mud, dirt and insects.</p>\r\n<p>20.7 Awesome reserves the right to retain a AU$100 fuel fee if the Vehicle is not returned with a full fuel tank.</p>\r\n<p>20.8 Our depots do not keep cash overnight and are unable to refund bonds as cash.</p>\r\n<p>Any cash bond due for return will transferred to a bank account   nominated by the Hirer. Any bank fees associated with this are to be   borne by the Hirer.</p>\r\n<p class=\"blue_header\">(21) COMPANY’S INDEMNITY (DAMAGE COVER)</p>\r\n<p>21.1 Subject to Clause 21.3, if the Vehicle is involved in a   collision with another motor vehicle (“the Incident”) during the Hire   Period (other than whilst you were reversing the Vehicle), then you may   purchase an indemnity (“the Indemnity”) from the Company by paying the   Liability Fee and providing an Incident Report to the Company within 24   hours of the Incident. By purchasing the Indemnity, you agree; (i) that   the Company may bring, defend or settle legal proceedings in relation to   the Incident in your name and shall have sole conduct of any related   proceedings or settlement negotiations; and (ii) any rights that You at   any time have or might have against any other person (except for the   personal injury) because of the Incident are assigned to the Company   (“the Rights”).</p>\r\n<p>21.2 If you purchase an Indemnity, the Company will indemnify you in   respect of any judgment of a court obtained by a third party against you   for any property loss or damage arising from that “Incident” up to a   limit of $50,000 (inclusive of legal cost and interest), except for loss   or damage of property in your possession. However, the company will not   indemnify you if you were not entitled to purchase an Indemnity at the   time the Liability Fee is paid by or charged to you or purportedly so   paid or charged, or if you breach this Agreement at any time after the   “Incident”.</p>\r\n<p>21.3 You are not entitled to purchase an Indemnity if You:</p>\r\n<p>(i) are in breach of this agreement at the time of or at any time after the “Incident”;</p>\r\n<p>(ii) are covered by any contract of insurance:</p>\r\n<p>(iii) have admitted fault or liability for the “Incident”;</p>\r\n<p>iv) have done or omitted to do anything that in the Company’s   unfettered opinion detrimentally affects any of the rights or its   ability to defend any legal proceedings; or</p>\r\n<p>(v) cannot or do not identify the other vehicle or driver involved in the “Incident”.</p>\r\n<p class=\"blue_header\">(22) PROCEDURES IN CASE OF ACCIDENT</p>\r\n<p>If the Customer is involved in a motor vehicle accident whilst on hire, the following procedures should be followed:</p>\r\n<p>a) AT THE ACCIDENT SCENE THE CUSTOMER MUST:</p>\r\n<p>1. Obtain the names and addresses of third parties and any witnesses.</p>\r\n<p>2. Report the accident to police, regardless of estimated damage costs.</p>\r\n<p>3. Not accept blame or insist the other party is at fault.</p>\r\n<p>4. If possible, photograph damage to all vehicle(s) and registration number(s).</p>\r\n<p>5. Phone the nearest Awesome Branch with the accident’s details within 24 hours.</p>\r\n<p>b) AT THE BRANCH:</p>\r\n<p>1. The Customer must produce their Driver’s Licence and hand over the   police report (if applicable) and any supporting photographs.</p>\r\n<p>2. The Customer is required to pay the excess (if applicable) and any   other amount due by them in respect to any damage arising from an   accident, loss, or damage. This amount is payable at the time of   reporting “the event” and not at completion of the Rental Period.</p>\r\n<p>3. The Awesome Customer Service Representative will ensure the Motor   Vehicle Accident Report is completed clearly and accurately signed by   the Customer.</p>\r\n<p>c) EXCHANGE VEHICLE:</p>\r\n<p>1. The availability of an Exchange Vehicle is not guaranteed;   provision is subject to availability, client location, accident   liability and remaining hire duration. Client charges may be incurred   (see below).</p>\r\n<p>2. If an exchange Vehicle is required as a result of an accident, the   Customer is responsible for making their own way to the nearest Awesome   branch or pick up location at their own expense.</p>\r\n<p>3. The Customer will pay for any costs relating to delivery of an   exchange vehicle as a result of any single vehicle accident. This charge   applies irrespective of any excess reduction taken.</p>\r\n<p>4. A new Security Bond will be required for the exchange vehicle.</p>\r\n<p>IMPORTANT NOTE:</p>\r\n<p>Under NO circumstances should the Customer attempt to start or drive a   vehicle that has been involved in an accident or wildlife accident,   damaged by roll-over, water submersion or by any other means without   permission from Awesome.</p>\r\n<p>If the vehicle is un-drivable after an accident and the Customer   would like to have a replacement vehicle, this will be subject to   availability, distance and time. In accordance with Clause 22 (c), the   Customer must at his/her own expense, make his/her own way to the   nearest Awesome Branch. If a replacement vehicle is available and   accepted by the Customer for the remainder of the Rental Period a new   Excess Reduction policy will be required.</p>\r\n<p class=\"blue_header\">(23) INFRINGEMENTS</p>\r\n<p>Awesome reserves the right to charge the Customer for any traffic or   parking fines or unpaid toll notices received; associated administration   costs and/or accidents including third party property damage not   reported on return of the Vehicle. The administration fee per incident   or fine/notice received will be AU$55.</p>\r\n<p class=\"blue_header\">(24) RENTAL CHARGES</p>\r\n<p>Total charges as set out in your rental agreement are not final. The   Customer will pay any shortfall in charges to Awesome and the Customer   will receive a refund for any over charges made by Awesome. Wherever   possible, any amendment to charges will be notified to the customer at   conclusion of rental, and the customer agrees to payment of any such   charges at the time.</p>\r\n<p class=\"blue_header\">(25) PAYMENT OF CHANGES – JOINT AND SEVERAL LIABILITIES</p>\r\n<p>All charges and expenses payable by the Customer under this Agreement   are due on demand by Awesome including any collection costs and   reasonable legal fees incurred by Awesome. When the Customer comprises   of more than one person, each person is liable jointly and severally for   all obligations of the Customer pursuant to this Agreement.</p>\r\n<p class=\"blue_header\">(26) CONDITIONAL UPON PAYMENT</p>\r\n<p>The Customer agrees that provision of any rental vehicle is   conditional upon Awesome being paid (prior to travel commencing) by the   Travel Agent or Travel Wholesaler who arranged the vehicle rental on the   Customer’s behalf.</p>\r\n<p class=\"blue_header\">(27) TERMINATING THE AGREEMENT &amp; REPOSSESSING THE VEHICLE</p>\r\n<p>27.1 The Customer acknowledges that Awesome may terminate this   Agreement and repossess the Vehicle (and for that purpose enter upon any   premises and remove the vehicle) at any time, without notification to   the customer, and that the Customer will pay the reasonable cost of   repossessing the Vehicle, including towing charges if:</p>\r\n<p>a) The Customer is in breach of any material term of this agreement, particularly Clauses 11 and 32;</p>\r\n<p>b) The Customer has obtained the Vehicle through fraud or is representation;</p>\r\n<p>c) The Vehicle appears to be abandoned;</p>\r\n<p>d) The Vehicle is not returned on the agreed return date or Awesome   reasonably believes that the Vehicle will not be returned on the agreed   return date; or</p>\r\n<p>e) Awesome considers, on reasonable grounds, that the safety of the passengers or the condition of the Vehicle is endangered.</p>\r\n<p>27.2 The Customer understands that in the event of such termination   or repossession, the Customer has no right to a refund of any part of   the rental charges or the Security Deposit.</p>\r\n<p class=\"blue_header\">(28) STAMP DUTY TAXES</p>\r\n<p>All prices quoted include 10% GST an additional 2.5% fee will be   added to the total rental charge to cover state government fees, duties   and associated costs.</p>\r\n<p class=\"blue_header\">(29) CREDIT CARD</p>\r\n<p>29.1 Awesome accepts payment by Visa and MasterCard, which attract a   merchant surcharge of 2.5%. Please note that the credit card   administration fee is non-refundable regardless, even if the   cancellation is made within the acceptable time period stated under   Awesome cancellation policy.</p>\r\n<p>29.2 If a credit card is presented as payment, the credit card holder will be jointly and severally liable as a Customer.</p>\r\n<p>29.3 When payment is made by credit card, the Customer agrees that:   Awesome is irrevocably authorized to complete any documentation and take   any other action to recover from the Customer’s credit card issuer all   amounts due by the Customer pursuant to this Agreement, including, but   not limited to, any amounts due in respect of damage to the Vehicle or   to property of a third party and all other additional charges as they   are incurred including all parking and traffic infringement penalties,   road toll fines and associated administration costs;</p>\r\n<p>29.4 The Customer will not dispute his/her liability to Awesome for   any amount properly due under this Agreement and the Customer shall   indemnify and keep indemnified Awesome against any loss incurred   (including legal costs) by reason of notifying the Customer’s credit   card issuer of such dispute; In the event that Awesome elect to accept   payment of the Security Deposit by holding a signed and authorized open   credit card voucher which is returned to the Customer at the completion   of the Rental Period, the Customer agrees that Awesome is entitled to   recover payment from the Customer’s credit card issuer pursuant to   paragraph (a) in respect of any amounts due which were not known at the   time of return of the voucher; and Awesome may process credit card   charges pertaining to the rental after the hire period.</p>\r\n<p>29.5 The Customer acknowledges that all transactions under this Agreement are conducted in Australian Dollars.</p>\r\n<p>29.6 Due to exchange rate fluctuations and bank fees, there may be a   variance between the amount initially debited against the Customer’s   credit card and the amount refunded.</p>\r\n<p class=\"blue_header\">(30) CANCELLATIONS</p>\r\n<p>If a booking is amended within the cancellation fee period and   subsequently cancelled, the cancellation fee for the original booking   will apply.</p>\r\n<p>The cancellation fees for vehicles are:</p>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Up to 7 days prior to pick up: 20% of Gross Rental</li>\r\n<li>If cancelled 6 - 1 days prior to pick up: 50% of Gross Rental</li>\r\n<li>On day of pick up or no show: 100% of Gross Rental</li>\r\n<li>If vehicle is returned early for any reason whatsoever: No refund available.</li>\r\n</ul>\r\n</div>\r\n<p class=\"blue_header\">(31) PROHIBITED AREAS &amp; ROAD RESTRICTIONS</p>\r\n<p>Normal two wheel drive campers are only to be driven on sealed roads.   Certain parts of Australia are designated as prohibited areas. Due to   the remoteness and conditions of roads, these areas are unsafe to travel   through and are prohibited at all times. Prohibited areas include:</p>\r\n<div class=\"general_listings\">\r\n<ul>\r\n<li>Central West Australia bounded by the Great Northern, Eyre and Stuart Highways.</li>\r\n<li>Central Eastern Australia bounded by the Stuart, Barrier, Mitchell, Landsborough &amp; Barkley Highways.</li>\r\n<li>Cape York north of Cooktown, the Carpentaria Coast and Arnhem Land bounded by the Barkley &amp; Stuart Highways.</li>\r\n<li>The Kimberley Ranges bounded the Great Northern Highway.</li>\r\n<li>Cape Leveque, the Bungle Bungles and the Lost City in Litchfield National Park.</li>\r\n<li>Any beach, sand or island.</li>\r\n<li>Awesome Campers and Vans travelling into the Karijni National Park will incur a fixed fee of $150.00</li>\r\n</ul>\r\n</div>\r\n<div>\r\n<p class=\"blue_header\">(32) CUSTOMER WARRANTIES</p>\r\n<p>The Customer warrants that all information supplied by them to Awesome in connection with this Agreement is true.</p>\r\n<p class=\"blue_header\">(33) ENTIRE AGREEMENT</p>\r\n<p>This Agreement constitutes the entire agreement of the parties and   there are no other oral undertakings, warranties or agreements between   the parties relating to the subject matter of this Agreement</p>\r\n<p class=\"blue_header\">(34) TOLL CHARGES</p>\r\n<p>All vehicles are fitted with an electronic E Tag. A Toll Charge fee   of $30.00 can be taken upon collection of the vehicle, otherwise a   $20.00 administration fee along with the toll charges will be charged to   a nominated credit card after the vehicle is returned..</p>\r\n</div>','',1,0,0,0,'2012-03-20 11:57:05',62,'','0000-00-00 00:00:00',0,0,'0000-00-00 00:00:00','2012-03-20 11:57:05','0000-00-00 00:00:00','','','show_title=1\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',1,0,3,'','',0,226,'robots=\nauthor='),(12,'Thanks','thanks','','<h2 class=\"onsubmit\">Thank you for submitting the form.</h2>','',1,0,0,0,'2012-03-20 12:11:03',62,'','2012-03-20 12:13:57',62,62,'2013-02-11 04:46:23','2012-03-20 12:11:03','0000-00-00 00:00:00','','','show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',3,0,2,'','',0,3,'robots=\nauthor='),(13,'Location Info','location-info','','<ul id=\"accordion\">\r\n<li> <a class=\"item popular\" href=\"#\" rel=\"popular\">Sydney NSW Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"860px\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">Kingsgrove<br /> Monday - Friday: 9:00am – 5:00pm<br /> Saturday: 9:00am – 2:00pm<br /> Sunday: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Kingsgrove,+New+South+Wales,+Australia&amp;aq=&amp;sll=-33.947807,151.097119&amp;sspn=0.060734,0.132093&amp;ie=UTF8&amp;hq=&amp;hnear=Kingsgrove+New+South+Wales,+Australia&amp;t=h&amp;z=14&amp;ll=-33.942174,151.101451&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n<li> <a class=\"item category\" href=\"#\" rel=\"category\">Perth WA Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">7 Aberdeen Place<br /> West Perth WA 6005<br /> Monday - Friday: 9:00am – 4:30pm<br /> Saturday: 9:00am – 1:00pm<br /> Sunday: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=7+Aberdeen+Place+West+PErth&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=59.119059,49.482422&amp;vpsrc=6&amp;ie=UTF8&amp;hq=&amp;hnear=7+Old+Aberdeen+Pl,+West+Perth+Western+Australia+6005,+Australia&amp;t=m&amp;ll=-31.941899,115.848148&amp;spn=0.016005,0.033023&amp;z=14&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Cairns QLD Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">203 Newell Street<br /> Cairns QLD 4870<br /> Monday - Friday: 9:00am – 3:00pm<br /> Weekend: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=203+Newell+Street+Cairns+QLD+4870&amp;aq=&amp;sll=-33.871609,151.205499&amp;sspn=0.015197,0.033023&amp;ie=UTF8&amp;hq=&amp;hnear=203+Newell+St,+Cairns+Queensland+4870,+Australia&amp;t=h&amp;z=14&amp;iwloc=A&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Brisbane QLD Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">269 Earnshaw Road(Entrance via Axford St)<br /> Northgate QLD 4013<br /> Monday - Friday: 9:00am – 5:00pm<br /> Saturday: 9:00am – 1:00pm<br /> Sunday: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=269+Earnshaw+Road,+Northgate,+Queensland,+Australia&amp;aq=&amp;sll=-27.385038,153.073599&amp;sspn=0.008126,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=269+Earnshaw+Rd,+Northgate+Queensland+4013,+Australia&amp;t=h&amp;z=14&amp;ll=-27.385038,153.073599&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Melbourne VIC Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">45 Bond Street<br /> Ringwood VIC 3134<br /> Monday – Friday: 8:00am – 5:00pm<br /> Saturday: 8:00am - 1:00pm<br /> Sunday: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com.au/maps?q=45+Bond+Street,+Ringwood,+Victoria&amp;sll=-37.814162,145.222660&amp;hl=en&amp;ie=UTF8&amp;hq=&amp;hnear=45+Bond+St,+Ringwood+Victoria+3134&amp;t=m&amp;vpsrc=0&amp;ll=-37.814192,145.222692&amp;spn=0.023732,0.036478&amp;z=14&amp;iwloc=A&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n<li> <a class=\"item comment\" href=\"#\" rel=\"comment\">Adelaide SA Office</a> \r\n<ul>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" valign=\"top\">91 Sir Donald Bradman Drive<br /> Hilton SA 5033<br /> Monday – Friday: 8:30am – 4:00pm<br /> Saturday: 9:00am - 12:00pm<br /> Sunday: Closed</td>\r\n<td align=\"right\" valign=\"top\"><iframe src=\"http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=91+Sir+Donald+Bradman+Drive,+Adelaide,+South+Australia,+Australia&amp;aq=0&amp;sll=-38.036816,145.340781&amp;sspn=0.007208,0.016512&amp;ie=UTF8&amp;hq=&amp;hnear=91+Sir+Donald+Bradman+Dr,+Adelaide+South+Australia+5033,+Australia&amp;t=h&amp;z=14&amp;ll=-34.93214,138.568101&amp;output=embed\" width=\"425\" height=\"350\" scrolling=\"no\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\"></iframe></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</ul>\r\n</li>\r\n</ul>\r\n<script src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js\" type=\"text/javascript\"></script>\r\n<script src=\"js/jquery.easing.1.3.js\" type=\"text/javascript\"></script>\r\n<script type=\"text/javascript\">// <![CDATA[\r\n	$(document).ready(function () {\r\n		$(\'#accordion a.item\').click(function () {\r\n			//slideup or hide all the Submenu\r\n			$(\'#accordion li\').children(\'ul\').slideUp(\'fast\');	\r\n			//remove all the \"Over\" class, so that the arrow reset to default\r\n			$(\'#accordion a.item\').each(function () {\r\n				if ($(this).attr(\'rel\')!=\'\') {\r\n					$(this).removeClass($(this).attr(\'rel\') + \'Over\');	\r\n				}\r\n			});\r\n			//show the selected submenu\r\n			$(this).siblings(\'ul\').slideDown(\'fast\');\r\n			//add \"Over\" class, so that the arrow pointing down\r\n			$(this).children(\'a\').addClass($(this).children(\'li a\').attr(\'rel\') + \'Over\');			\r\n			return false;\r\n		});\r\n	});\r\n// ]]></script>','',0,0,0,0,'2012-03-20 09:55:19',62,'','2012-03-20 11:37:39',62,0,'0000-00-00 00:00:00','2012-03-20 09:55:19','0000-00-00 00:00:00','','','show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=',0,0,1,'','',0,0,'robots=\nauthor=');
/*!40000 ALTER TABLE `jos_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_content_frontpage`
--

DROP TABLE IF EXISTS `jos_content_frontpage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_content_frontpage`
--

LOCK TABLES `jos_content_frontpage` WRITE;
/*!40000 ALTER TABLE `jos_content_frontpage` DISABLE KEYS */;
INSERT INTO `jos_content_frontpage` VALUES (1,1);
/*!40000 ALTER TABLE `jos_content_frontpage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_content_rating`
--

DROP TABLE IF EXISTS `jos_content_rating`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_content_rating`
--

LOCK TABLES `jos_content_rating` WRITE;
/*!40000 ALTER TABLE `jos_content_rating` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_content_rating` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_acl_aro`
--

DROP TABLE IF EXISTS `jos_core_acl_aro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_acl_aro`
--

LOCK TABLES `jos_core_acl_aro` WRITE;
/*!40000 ALTER TABLE `jos_core_acl_aro` DISABLE KEYS */;
INSERT INTO `jos_core_acl_aro` VALUES (10,'users','62',0,'Administrator',0),(11,'users','63',0,'-',0);
/*!40000 ALTER TABLE `jos_core_acl_aro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_acl_aro_groups`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_acl_aro_groups`
--

LOCK TABLES `jos_core_acl_aro_groups` WRITE;
/*!40000 ALTER TABLE `jos_core_acl_aro_groups` DISABLE KEYS */;
INSERT INTO `jos_core_acl_aro_groups` VALUES (17,0,'ROOT',1,22,'ROOT'),(28,17,'USERS',2,21,'USERS'),(29,28,'Public Frontend',3,12,'Public Frontend'),(18,29,'Registered',4,11,'Registered'),(19,18,'Author',5,10,'Author'),(20,19,'Editor',6,9,'Editor'),(21,20,'Publisher',7,8,'Publisher'),(30,28,'Public Backend',13,20,'Public Backend'),(23,30,'Manager',14,19,'Manager'),(24,23,'Administrator',15,18,'Administrator'),(25,24,'Super Administrator',16,17,'Super Administrator');
/*!40000 ALTER TABLE `jos_core_acl_aro_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_acl_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_acl_aro_map`
--

LOCK TABLES `jos_core_acl_aro_map` WRITE;
/*!40000 ALTER TABLE `jos_core_acl_aro_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_core_acl_aro_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_acl_aro_sections`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_acl_aro_sections`
--

LOCK TABLES `jos_core_acl_aro_sections` WRITE;
/*!40000 ALTER TABLE `jos_core_acl_aro_sections` DISABLE KEYS */;
INSERT INTO `jos_core_acl_aro_sections` VALUES (10,'users',1,'Users',0);
/*!40000 ALTER TABLE `jos_core_acl_aro_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_acl_groups_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_groups_aro_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_acl_groups_aro_map`
--

LOCK TABLES `jos_core_acl_groups_aro_map` WRITE;
/*!40000 ALTER TABLE `jos_core_acl_groups_aro_map` DISABLE KEYS */;
INSERT INTO `jos_core_acl_groups_aro_map` VALUES (25,'',10),(25,'',11);
/*!40000 ALTER TABLE `jos_core_acl_groups_aro_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_log_items`
--

DROP TABLE IF EXISTS `jos_core_log_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_log_items`
--

LOCK TABLES `jos_core_log_items` WRITE;
/*!40000 ALTER TABLE `jos_core_log_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_core_log_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_core_log_searches`
--

DROP TABLE IF EXISTS `jos_core_log_searches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_core_log_searches`
--

LOCK TABLES `jos_core_log_searches` WRITE;
/*!40000 ALTER TABLE `jos_core_log_searches` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_core_log_searches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_groups`
--

DROP TABLE IF EXISTS `jos_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_groups`
--

LOCK TABLES `jos_groups` WRITE;
/*!40000 ALTER TABLE `jos_groups` DISABLE KEYS */;
INSERT INTO `jos_groups` VALUES (0,'Public'),(1,'Registered'),(2,'Special');
/*!40000 ALTER TABLE `jos_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_menu`
--

DROP TABLE IF EXISTS `jos_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_menu`
--

LOCK TABLES `jos_menu` WRITE;
/*!40000 ALTER TABLE `jos_menu` DISABLE KEYS */;
INSERT INTO `jos_menu` VALUES (1,'mainmenu','Home','home','index.php?option=com_content&view=frontpage','component',1,0,20,0,1,0,'0000-00-00 00:00:00',0,0,0,3,'num_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,1),(2,'topmenu','Campervans','campervans','index.php?option=com_content&view=article&id=2','component',1,0,20,0,1,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(3,'topmenu','Insurances','insurances','index.php?option=com_content&view=article&id=3','component',0,0,20,0,2,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(4,'extramenu','Awesome 2 Person Classic Campervan','awesome-2-person-classic-campervan','index.php?option=com_content&view=article&id=4','component',1,0,20,0,1,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(5,'extramenu','Awesome 2 Person Deluxe Campervan','awesome-2-person-deluxe-campervan','index.php?option=com_content&view=article&id=5','component',1,0,20,0,2,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(6,'extramenu','Awesome 2 Person Super Deluxe Campervan','awesome-2-person-super-deluxe-campervan','index.php?option=com_content&view=article&id=6','component',1,0,20,0,3,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(7,'extramenu','3 Person Classic Campervan','3-person-classic-campervan','index.php?option=com_content&view=article&id=7','component',1,0,20,0,4,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(8,'extramenu','3 Seater Super Deluxe Campervan','3-seater-super-deluxe-campervan','index.php?option=com_content&view=article&id=8','component',1,0,20,0,5,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(9,'extramenu','4 Person Super Deluxe Campervan with Roof Top Tent','4-person-super-deluxe-campervan-with-roof-top-tent','index.php?option=com_content&view=article&id=9','component',1,0,20,0,6,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(10,'topmenu','Location Info','location-info','index.php?option=com_content&view=article&id=10','component',1,0,20,0,3,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(11,'topmenu','Contact Us','contact-us','index.php?option=com_chronocontact','component',1,0,34,0,4,0,'0000-00-00 00:00:00',0,0,0,0,'formname=contactUs\nshowtipoftheday=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(12,'topmenu','Terms & Conditions','terms-a-conditions','index.php?option=com_content&view=article&id=11','component',1,0,20,0,5,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0),(13,'extramenu','Thanks','thank','index.php?option=com_content&view=article&id=12','component',1,0,20,0,7,0,'0000-00-00 00:00:00',0,0,0,0,'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n',0,0,0);
/*!40000 ALTER TABLE `jos_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_menu_types`
--

DROP TABLE IF EXISTS `jos_menu_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_menu_types`
--

LOCK TABLES `jos_menu_types` WRITE;
/*!40000 ALTER TABLE `jos_menu_types` DISABLE KEYS */;
INSERT INTO `jos_menu_types` VALUES (1,'mainmenu','Main Menu','The main menu for the site'),(2,'topmenu','Top Menu',''),(3,'extramenu','Extra Menu','');
/*!40000 ALTER TABLE `jos_menu_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_messages`
--

DROP TABLE IF EXISTS `jos_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_messages`
--

LOCK TABLES `jos_messages` WRITE;
/*!40000 ALTER TABLE `jos_messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_messages_cfg`
--

DROP TABLE IF EXISTS `jos_messages_cfg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_messages_cfg`
--

LOCK TABLES `jos_messages_cfg` WRITE;
/*!40000 ALTER TABLE `jos_messages_cfg` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_messages_cfg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_migration_backlinks`
--

DROP TABLE IF EXISTS `jos_migration_backlinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_migration_backlinks`
--

LOCK TABLES `jos_migration_backlinks` WRITE;
/*!40000 ALTER TABLE `jos_migration_backlinks` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_migration_backlinks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_modules`
--

DROP TABLE IF EXISTS `jos_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_modules`
--

LOCK TABLES `jos_modules` WRITE;
/*!40000 ALTER TABLE `jos_modules` DISABLE KEYS */;
INSERT INTO `jos_modules` VALUES (1,'Main Menu','',0,'topMenu',0,'0000-00-00 00:00:00',0,'mod_mainmenu',0,0,0,'menutype=mainmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n',1,0,''),(2,'Login','',1,'login',0,'0000-00-00 00:00:00',1,'mod_login',0,0,1,'',1,1,''),(3,'Popular','',3,'cpanel',0,'0000-00-00 00:00:00',1,'mod_popular',0,2,1,'',0,1,''),(4,'Recent added Articles','',4,'cpanel',0,'0000-00-00 00:00:00',1,'mod_latest',0,2,1,'ordering=c_dsc\nuser_id=0\ncache=0\n\n',0,1,''),(5,'Menu Stats','',5,'cpanel',0,'0000-00-00 00:00:00',1,'mod_stats',0,2,1,'',0,1,''),(6,'Unread Messages','',1,'header',0,'0000-00-00 00:00:00',1,'mod_unread',0,2,1,'',1,1,''),(7,'Online Users','',2,'header',0,'0000-00-00 00:00:00',1,'mod_online',0,2,1,'',1,1,''),(8,'Toolbar','',1,'toolbar',0,'0000-00-00 00:00:00',1,'mod_toolbar',0,2,1,'',1,1,''),(9,'Quick Icons','',1,'icon',0,'0000-00-00 00:00:00',1,'mod_quickicon',0,2,1,'',1,1,''),(10,'Logged in Users','',2,'cpanel',0,'0000-00-00 00:00:00',1,'mod_logged',0,2,1,'',0,1,''),(11,'Footer','',0,'footer',0,'0000-00-00 00:00:00',1,'mod_footer',0,0,1,'',1,1,''),(12,'Admin Menu','',1,'menu',0,'0000-00-00 00:00:00',1,'mod_menu',0,2,1,'',0,1,''),(13,'Admin SubMenu','',1,'submenu',0,'0000-00-00 00:00:00',1,'mod_submenu',0,2,1,'',0,1,''),(14,'User Status','',1,'status',0,'0000-00-00 00:00:00',1,'mod_status',0,2,1,'',0,1,''),(15,'Title','',1,'title',0,'0000-00-00 00:00:00',1,'mod_title',0,2,1,'',0,1,''),(16,'Top Menu','',2,'topMenu',0,'0000-00-00 00:00:00',1,'mod_mainmenu',0,0,0,'menutype=topmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n',0,0,''),(17,'social media','<ul>\r\n<li><a href=\"http://www.awesomecampers.com.au\">View Full Site</a></li>\r\n<li class=\"right\"><a href=\"http://www.facebook.com/Awesomecampers\" target=\"new\" title=\"Facebook\"><img src=\"images/stories/campre/f_logo.png\" border=\"0\" width=\"24\" height=\"24\" /></a> <a href=\"http://twitter.com/awesomecamper\" target=\"new\" title=\"Twitter\"><img src=\"images/stories/campre/twitter-bird-dark-bgs.png\" border=\"0\" width=\"24\" height=\"24\" /></a></li>\r\n</ul>',0,'socialMedia',0,'0000-00-00 00:00:00',1,'mod_custom',0,0,0,'moduleclass_sfx=\n\n',0,0,'');
/*!40000 ALTER TABLE `jos_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_modules_menu`
--

DROP TABLE IF EXISTS `jos_modules_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_modules_menu`
--

LOCK TABLES `jos_modules_menu` WRITE;
/*!40000 ALTER TABLE `jos_modules_menu` DISABLE KEYS */;
INSERT INTO `jos_modules_menu` VALUES (1,0),(16,0),(17,0);
/*!40000 ALTER TABLE `jos_modules_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_newsfeeds`
--

DROP TABLE IF EXISTS `jos_newsfeeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_newsfeeds`
--

LOCK TABLES `jos_newsfeeds` WRITE;
/*!40000 ALTER TABLE `jos_newsfeeds` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_newsfeeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_plugins`
--

DROP TABLE IF EXISTS `jos_plugins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_plugins`
--

LOCK TABLES `jos_plugins` WRITE;
/*!40000 ALTER TABLE `jos_plugins` DISABLE KEYS */;
INSERT INTO `jos_plugins` VALUES (1,'Authentication - Joomla','joomla','authentication',0,1,1,1,0,0,'0000-00-00 00:00:00',''),(2,'Authentication - LDAP','ldap','authentication',0,2,0,1,0,0,'0000-00-00 00:00:00','host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),(3,'Authentication - GMail','gmail','authentication',0,4,0,0,0,0,'0000-00-00 00:00:00',''),(4,'Authentication - OpenID','openid','authentication',0,3,0,0,0,0,'0000-00-00 00:00:00',''),(5,'User - Joomla!','joomla','user',0,0,1,0,0,0,'0000-00-00 00:00:00','autoregister=1\n\n'),(6,'Search - Content','content','search',0,1,1,1,0,0,'0000-00-00 00:00:00','search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),(7,'Search - Contacts','contacts','search',0,3,1,1,0,0,'0000-00-00 00:00:00','search_limit=50\n\n'),(8,'Search - Categories','categories','search',0,4,1,0,0,0,'0000-00-00 00:00:00','search_limit=50\n\n'),(9,'Search - Sections','sections','search',0,5,1,0,0,0,'0000-00-00 00:00:00','search_limit=50\n\n'),(10,'Search - Newsfeeds','newsfeeds','search',0,6,1,0,0,0,'0000-00-00 00:00:00','search_limit=50\n\n'),(11,'Search - Weblinks','weblinks','search',0,2,1,1,0,0,'0000-00-00 00:00:00','search_limit=50\n\n'),(12,'Content - Pagebreak','pagebreak','content',0,10000,1,1,0,0,'0000-00-00 00:00:00','enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),(13,'Content - Rating','vote','content',0,4,1,1,0,0,'0000-00-00 00:00:00',''),(14,'Content - Email Cloaking','emailcloak','content',0,5,1,0,0,0,'0000-00-00 00:00:00','mode=1\n\n'),(15,'Content - Code Hightlighter (GeSHi)','geshi','content',0,5,0,0,0,0,'0000-00-00 00:00:00',''),(16,'Content - Load Module','loadmodule','content',0,6,1,0,0,0,'0000-00-00 00:00:00','enabled=1\nstyle=0\n\n'),(17,'Content - Page Navigation','pagenavigation','content',0,2,1,1,0,0,'0000-00-00 00:00:00','position=1\n\n'),(18,'Editor - No Editor','none','editors',0,0,1,1,0,0,'0000-00-00 00:00:00',''),(19,'Editor - TinyMCE','tinymce','editors',0,0,1,1,0,0,'0000-00-00 00:00:00','mode=advanced\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=raw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\ncontent_css=1\ncontent_css_custom=\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\ntoolbar=top\ntoolbar_align=left\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchreplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\nadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\ncustom_plugin=\ncustom_button=\n\n'),(20,'Editor - XStandard Lite 2.0','xstandard','editors',0,0,0,1,0,0,'0000-00-00 00:00:00',''),(21,'Editor Button - Image','image','editors-xtd',0,0,1,0,0,0,'0000-00-00 00:00:00',''),(22,'Editor Button - Pagebreak','pagebreak','editors-xtd',0,0,1,0,0,0,'0000-00-00 00:00:00',''),(23,'Editor Button - Readmore','readmore','editors-xtd',0,0,1,0,0,0,'0000-00-00 00:00:00',''),(24,'XML-RPC - Joomla','joomla','xmlrpc',0,7,0,1,0,0,'0000-00-00 00:00:00',''),(25,'XML-RPC - Blogger API','blogger','xmlrpc',0,7,0,1,0,0,'0000-00-00 00:00:00','catid=1\nsectionid=0\n\n'),(27,'System - SEF','sef','system',0,1,1,0,0,0,'0000-00-00 00:00:00',''),(28,'System - Debug','debug','system',0,2,1,0,0,0,'0000-00-00 00:00:00','queries=1\nmemory=1\nlangauge=1\n\n'),(29,'System - Legacy','legacy','system',0,3,0,1,0,0,'0000-00-00 00:00:00','route=0\n\n'),(30,'System - Cache','cache','system',0,4,0,1,0,0,'0000-00-00 00:00:00','browsercache=0\ncachetime=15\n\n'),(31,'System - Log','log','system',0,5,0,1,0,0,'0000-00-00 00:00:00',''),(32,'System - Remember Me','remember','system',0,6,1,1,0,0,'0000-00-00 00:00:00',''),(33,'System - Backlink','backlink','system',0,7,0,1,0,0,'0000-00-00 00:00:00',''),(34,'System - Mootools Upgrade','mtupgrade','system',0,8,0,1,0,0,'0000-00-00 00:00:00','');
/*!40000 ALTER TABLE `jos_plugins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_poll_data`
--

DROP TABLE IF EXISTS `jos_poll_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_poll_data`
--

LOCK TABLES `jos_poll_data` WRITE;
/*!40000 ALTER TABLE `jos_poll_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_poll_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_poll_date`
--

DROP TABLE IF EXISTS `jos_poll_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_poll_date`
--

LOCK TABLES `jos_poll_date` WRITE;
/*!40000 ALTER TABLE `jos_poll_date` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_poll_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_poll_menu`
--

DROP TABLE IF EXISTS `jos_poll_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_poll_menu`
--

LOCK TABLES `jos_poll_menu` WRITE;
/*!40000 ALTER TABLE `jos_poll_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_poll_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_polls`
--

DROP TABLE IF EXISTS `jos_polls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_polls`
--

LOCK TABLES `jos_polls` WRITE;
/*!40000 ALTER TABLE `jos_polls` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_polls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_sections`
--

DROP TABLE IF EXISTS `jos_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_sections`
--

LOCK TABLES `jos_sections` WRITE;
/*!40000 ALTER TABLE `jos_sections` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_session`
--

DROP TABLE IF EXISTS `jos_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_session`
--

LOCK TABLES `jos_session` WRITE;
/*!40000 ALTER TABLE `jos_session` DISABLE KEYS */;
INSERT INTO `jos_session` VALUES ('','1385616016','6a683e0b42090440ddf6a993e5766e55',1,0,'',0,0,'MySn0lbngUhRvqZ6vwNcylkWBRh_z9lBeCxeoMkpuY4_ODD1gsNjVmmAovIKwVXP7ouFsKy00WoYYWsQGPDFeT6R5ZJtr1vp6XzTWGIn473XENwtZY8CNAF6phU4CGejB8FrUliDnMM40XLuC1TiGNlVzGq8upu-NoRCYCyvutnJDuWKDUPU5PD5-VMWJCT3fpimJvDTTG1ZqN8Jv4JZ3ebfN8y-DDIdrphneV_P5ptomglxfZQNNnPHufJvMNxrHTOlQbGHLoEvqddg1l2d29qeAR-b4nNOLnC7xypfFnC-5X6ou8YSBj3L0vws51BctH4va5u_ciJoR6RMIlb0dP0-oOghCRGJXOqmBtS_BVFVUBt8FIQJTue0MCk4ClFK5wSRPU6GD42d_Bs9b8B5MMhnUXGXUh7-8KCM9_CJbh-fVO0Oj3PbE_27KYSfiI0gg1Ah3mdIk_iGlTkeGyOvi5riE2G9T-McKsiXYAfm6Wd3o3L5xzLA9TmbRNLYQHich2m-25dp1cItSu6-QgjdoYEx8s4Bjt_q-0zNHHysDysllmXj5DYfy5cybyqJg42pdbQcSOQyaAZeh7lfzH9v0N1noW54Lvs_C5aamYydJ-PYX_wvN9h9Gi2CPKHpvC16ZZ9RiTtByuotQBf5-jaBs4Y0QI2SP2SQSVq18wbipC4t1oE0jnCiHta_ioMVBzgnLYC5oPoqjEIRfUZz0ydoCmsfdLRFMKMPRPUk8BD_Phxd5w2xNiBe4eKMr01fJW7UQs8pqA899ab_Cqzd0aMcSXACgPIl-yyILsPNE1kmLZ2TstJfeseJBcpGhMKKeoNpob8UEYA631S7_JGv5IkP3g7znaW9ByWqwGCkdWaHfO68ZWZ3LoEuAXhxs2m3Q_mub9JrtjZ8oNT9OvFKWnET2pB22XD-DzDqL_kmIEFh4WhX_oeMSrqgb19KTklkY3JqApE5rKbAXzEe-9Ps5vTuQkQk3S3YVcSlAwzynWyl1F9fSvA7fGFGTacnM7OmsW4bPhFkKEADGsaBKLC8JH3ok1TKM-E7vVtwnFEomulzRXVWL8xGvxbvf0mKOMVCJi7qYvTG26f3kejI8roE8iHUzFSlgLEqAW8azlkLaNwi_eFhDtdxwL6Q_g1nWzNOSnZiwXlDFfF9gEtXxmTB7TnJbq0Tv53DhEpPEWDnrpGF4yminDdRUbcvgmSYcXHa7yAHccOX1WKLyUvLgNK9MptjuQD-Gd6C6ko6iQyTLeeJcxMMGrIzc6Pu_c4xCPRstKmEG3GDuk-uD_9E1hwqg-DNBs1aBPiVwg-Mm3qBdtTSYMHtxKPWzt2ACJ4Gvc7bqT7IuvCrQk4sH0BTqun4RIO9-OxyhQsML5I0sjOehEF0_MOgdd8AGws5rJNf7M7UWtntXk3UwzvjNG6SPNEBsym2iu7gYi-nj6sl3Okmc2XftSJ3Vu2dre6GrWzZm_TG9Y1kCUejNJp0SRpyZ2jjcRmpft9BQxWvykUHKkxpH4EDHAkI520lOja0YRU0xqOTonHDQP7WJ_JMHdYbn9MShZ2YAqcG6fOC0iaLBDZtKYPUsK3sc5ex7qFEUYC19TzQPXGPRJ3wqgyaj7a0k9JGZB4jROZCFYXo5kviGF5reX3EQLU1PA6oHdu4otIxCD6N3hhsRVdVBKEjyBzVbiX-MZ6srA..'),('','1385615234','ff8357cea29d907da0ea87f22844703d',1,0,'',0,0,'TNGdbOH14G4Bdx1hsBZC-kkfFkziLQcbTjryIfVd1qD9nsTEFQINE0taUsep2iew58RiuPKIjvNsIWcT0EFo39WjTvxlEQftoBlelZxescgYuavmp3JnlS36p2-dmJh78vV5rJhJwOF9v7K7EQ9M3HNTZKWMwM3Qro62exseVYkSVxwGCtu_tlZ2tWTfAsMF2DhKfNEomtRocDyA_0eIKqXyoqIwBUpNRu1i3f3fKPY5hue7850hZb6n2LiUEUMGsDIIt_lnyW-oqdkDQfqP3vP3N5PNUrUeVWps-33SbzONKxPTp5m__Cu9ucBLKHVjyBiGOe8vS-AhwBsyxW-IUzTq8HpJUe6QaGEFMSgRXBXpsm_-hLQvzjERrIenA_OqphG5KmPJlhoXjxmhY-YndaJUGNqwIpGIx6_0E1jHo8aFLlAPyeZD0slyQp0fYeFq_p1DqUEmdLJGrNGAiGxJ6ZA8s9guK56m2Lwz2rI9vQ5xG2SdUd6HEaUrCgkI1xuK1YhVFzG89iKV5VMXv8GjUv5wwarJYB3JP1mTa550BVvcptRbsXr7DbPvnqbJBIjRkqVbEiiaRqc3ktK3lGvV7I6cOQkg_7Xo_xybDH_jH3X9QsXm6QKcW6LNUQ8Ema36fa1-7itIGYUCMgKLEL7W0aN-MgM8qtlLWMut6RAukdfNWKALHweEII7ZlQ5PpVCXdbjIiAajaxhTHMNJHHTBH0cifccao_xvpcLQkZnMcbNoOvQutoRHnPJAHXQXS7TyDJL0ACRnsSj2sB7bmUY00fDvDUDF26vhdO9pS8lT0k9FglDhSRPcRmpGRz8Kt1_KbV3C3A6vYDE2Zs3mYIW3uXsT5yJ1yXDnFl8o7YQbLm-6tUHsPkC-nshZmVt3AFWlRhETZIMruW_NVy4pfWVYdMVPYgswzvmtoGPHCUZTfDqw1KrukifAGO80l3EMU1dfpwNzlhsF57ZfEfiqBdZyElilsG6UAhzHrgc1EgSvz11WQmA5pLKGgx5NW6-8UstVAezZF3BiCFb_94bEE6O5NR7ruLv__n6RuNJuNb9UI7uDv16ZW6jR6qZjCmPJ8oZirVlwdAaFJtdi5MXNKnpuwPv16MO5-VHeWQ1Yj8263aSEhptk-ZzYDa7wHC_E1HqKJVuIGcoxeGWLYRPUmQO-Slr4ZqF2uOW1JRMi_4YEbZsVkh8runlYAtLaug0T0QhMgHBqy9SIHLWqCApWkg6aWXCjUjavf9XaJg_itThCPRt-z1JMLH_QkQ6VI_RaVaFmYENQQhGBFM8T7vjOz3Y68LywgqPWkHnVOYEWmrOV6lnSmuUIkdDft_oCVjkptUbNsUmfVF18jWp74Q4oS3A3ldYMzWDrqJcr9gDob1NKc5zVgmNC04g9xdCe9tRSlof9u-WXvLjREKUDjq5g6aAsGr0uoy34JiFyd8Tr7c9upEBWWWP51Z3-25B8bQvK6JBREsg_Y5Bfk-HKbAJG34wMiQ..'),('','1385615255','2056f7c99f15b8cbee9d881d9beeb0c9',1,0,'',0,0,'BbDxomGUcVlVFynHNbmDsakiPnwMGf2ashmP2jFgkBkKhX2CWiZ3pkNr_3GBHdugvk51uJWuM23DaiTw9EKR0-HIwCCYhzAJFqKs_2ZbJ3bams7dlKGB2tDM1Gdkrqm312Uns6tTpxxspP6PSBZFjWWedecoIoHzO-NkWrY8G34DPrivG5GvkJsEgawiyofGA8eP_tO-xDu9q-Tj-nGeCdWUlM5AhEnEratpNzobPCMsjpQqpz4WkUZir63_ElPYHlH_Y5JRPo2284rYQbk5eUSSckXoth12b56KA-ZOhWBQbYhzDRDMeP6TZzR76IVUZtv4G0_N46HAGCpVo047Q5HUZt9D53S8Xxlaj3gM1OpbzbBmVB4-ZKuHAHxZ1t6o6ZrBZZyAPScM3D-j_Bbk1IE_29deH11kPa6xko7v8cW8RwBPaWS8jWfhUhIvfGuocTYIlBIaPG2hZjTa-lfdjO7MjH_am9MXzjoRKM-2ihYPl_LMB44h85guXjh69ltYpt2eJ3Gbx4zBbbt6B3p7sNAbUANp0TJLvaXtwszHxfWyySIYJv9dsRJYzL2dxRP3jV6xBnWPtUgE2JSraGBJvRGLo8WtM5_cKUjt6ky3NkI59_0i51Grda0kss8DUO3FfrOr-hNzT3mk5xzAN062jWTLXYb2h1ItKnNJObjCVmKlDhsXLX3uR0XtMwneLemq3V0bDj3GMgTfUCNAeva2o9aab_uQ82EcRxzbGvydYpZBHbajwyZxVqKezGvNfClQbL1BbMdVBvVh6FBIt56tXEuPmWJUulk9_72emde4eYDNNO_Xx8-XwwOUSEg8EDy_jUBlfxpZLLh7c45RbhfwNTCHPcZ5xALVDJBRYPF2Epe1YmN8pqYKk5AWHooLJcxCzQciS2zsJpkNxeu0ieKuY1T4Glqke45bJvdlh6v_2XQhtq4SvrML4kOhR2f7sI7bklJ6mhyc6eDyszw0-3Qpt7dylMJnz1BFEz5bMvPLGubd6g5ADnWdhh8slofU0cZK-09XaXejB9LJvk4m5wVTzBoXz3zWMSisyLyVgD5E6jHfIXARAvsgfjJruaxaFeHrhrbPkbK0w3rcqdsSyUXoxkkZIjkJNA9ZCDAbyts-N5fsomrbWZ1OAbLMtS39DYSsoWDeyyzuRrM36BgE2Ygt-eI0vhezacnFRSPJWN3F-c_t0ggGg97gfC9UeAxD8zRna1ySikHP5tV3CN8iJiPWpAfCPSU5o004eym2XG9K1aAiicRJcz_nzzXQsJLzOjMOK9fy7DWcKO21CBuhNzRMrVnIcZnQk90DJiwT9gbCcXcw2s-38pZMAuIDUxXyrASQPdV8mURTNA4YLciUMwwqTCIBkbRBh_3LMH3QfacRnI_8m_wFTPWXf0UshudNAZr7LKUA2NfAQu5PNF4SSx_pfWJlJ5rUOEW6cLg1rzioTDI9ykDLuc9_hfzqfwhbsz6gG-V9n3dC4yTB7FRyzhcDqFTd5m6XX2tCz0l_tTATMU1i8UyL5R7FnnIe-0lW-LpVDi-DR0W57HWolZw1YYhbuPz9OTnfv3tsu1ZR9wp4wrc.');
/*!40000 ALTER TABLE `jos_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_stats_agents`
--

DROP TABLE IF EXISTS `jos_stats_agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_stats_agents`
--

LOCK TABLES `jos_stats_agents` WRITE;
/*!40000 ALTER TABLE `jos_stats_agents` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_stats_agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_templates_menu`
--

DROP TABLE IF EXISTS `jos_templates_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_templates_menu`
--

LOCK TABLES `jos_templates_menu` WRITE;
/*!40000 ALTER TABLE `jos_templates_menu` DISABLE KEYS */;
INSERT INTO `jos_templates_menu` VALUES ('camperhireinaustralia',0,0),('khepri',0,1);
/*!40000 ALTER TABLE `jos_templates_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_users`
--

DROP TABLE IF EXISTS `jos_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_users`
--

LOCK TABLES `jos_users` WRITE;
/*!40000 ALTER TABLE `jos_users` DISABLE KEYS */;
INSERT INTO `jos_users` VALUES (1,'admin','admin','charles@onlinestart.com','4297f44b13955235245b2497399d7a93','Super Administrator',0,1,25,'2012-03-16 11:58:36','2013-07-15 07:53:59','1','');
/*!40000 ALTER TABLE `jos_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jos_weblinks`
--

DROP TABLE IF EXISTS `jos_weblinks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jos_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jos_weblinks`
--

LOCK TABLES `jos_weblinks` WRITE;
/*!40000 ALTER TABLE `jos_weblinks` DISABLE KEYS */;
/*!40000 ALTER TABLE `jos_weblinks` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-28 16:36:45
